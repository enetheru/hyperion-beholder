/*
 * The idea of this library is that it supplies core components to the
 * interfaces so that custom interfaces can use this for thr primary
 * functionality and deal with callbacks etc.
 *
 * things lik network connect and disconnect on idle.
 * some widgets which are common.
 * perhaps a function pointer for callback handling. etc.
 *
 */

/* path manupilation functions provided by this stack overflow answer
 * https://stackoverflow.com/a/29939805/1397944
 */
function basename(str, sep = '/') {
    return str.substr(str.lastIndexOf(sep) + 1);
}

function strip_extension(str) {
    return str.substr(0,str.lastIndexOf('.'));
}

function load_css( url, in_opt = {} ){
    //deal with default options
    defaults = { reload: false, after: null};
    let opt = { ...defaults, ...in_opt };
    /* creates a link element in the head of the document
     * reload = true; removes old, and loads in place. */

    // Pseudocode
    // Find existing link element with the same URL
    // if exists, and reload = false, return;
    // if exists and reload = true, delete
    // fetch css
    // run then function
}

function load_html( element, url, then = null ){
    /* fills the element with the contents of the url */

    // Pseudocode
    // fetch html
    // run then function
}
