import "/lib/d3-dispatch.js";

export {
    section,
    value,
    options,
    list
};

/* Template
 * --------
function template( id = ''){
    var _id = id,
        _label,
        _txHandle = txHandle;

    function widget( selection ){
        var widget = selection.append('div')
            .classed('template', true )
            .attr( 'id', _id )
            .text( _label )
            .on('tx', txHandle )
            .on('rx', rxHandle )
            .on('keydown', keydownHandle );
    }

    function rxHandle() {
        console.log( d3.select(this) );
        console.log( d3.event.detail );
    }

    function txHandle() {
        let widget = d3.select(this);
        console.log('default txhandle');
    }

    function keydownHandle(){
        //Filter and validate input here
        if( d3.event.which == 13 ) { //enter key
            d3.select(d3.event.target.parentNode).attr('state', 'changed');
            d3.selectAll(`#${_id}`).dispatch('tx',
                { detail: {value: d3.event.target.innerHTML } } );

            d3.event.preventDefault();
        }
        if( d3.event.which == 27 ){ //escape key
            d3.select(d3.event.target.parentNode).attr('state', null);
        }
    }

    widget.id      = function(v) { if(! arguments.length )return _id;      _id      = v; return widget; }
    widget.label   = function(v) { if(! arguments.length )return _label;   _label   = v; return widget; }
    widget.txHandle= function(v) { if(! arguments.length )return _txHandle;_txHandle= v; return widget; }

    return widget;
}
*/


/* Collapsible Section Widget
 * -------------------------- */
function section( id = '' ){
    var _id = id,
        _title,
        _expand = false,
        _depth = 0;//FIXME find a way to do this automatically

    function widget( selection ){
        var section = selection.append('section')
            .attr('id', _id )
            .classed('section', true );

        //Create titlebar
        var titlebar = section.append('div')
            .attr('id', 'titlebar')
            .on('click', function() {
                _expand = !_expand;
                var section = d3.select(this.parentNode);
                section.select('.icon').classed('rotate90', _expand );
                section.select('#content').classed('hide', !_expand );
            });

        // Indenting
        for( var i = 0; i < _depth; ++i ){
            titlebar.append('div')
                .classed('indent', true );
        }

        //Icon
        titlebar.append('div')
            .classed('icon', true )
            .classed('rotate90', _expand )
            .text('▸');

        //Title
        titlebar.append('h1').text( _title );

        //Separator
        titlebar.append('div').classed('separator', true);

        //create content area
        section.append('div')
            .attr('id', 'content')
            .classed('hide', !_expand );
    }

    /* Get/Set functions */
    widget.id     = function(v) { if(! arguments.length )return _id;     _id     = v; return widget; }
    widget.title  = function(v) { if(! arguments.length )return _title;  _title  = v; return widget; }
    widget.expand = function(v) { if(! arguments.length )return _expand; _expand = v; return widget; }
    widget.depth  = function(v) { if(! arguments.length )return _depth;  _depth  = v; return widget; }

    return widget;
}

/* Value Field
 * -----------*/
function value( id = 'nothing' ){
    var _id = id,
        _label,
        _type = 'value', // options: text, integer
        _prefix, _suffix,
        _default = '-',
        _edit = true,
        _wide = false,
        _txHandle = txHandle;

    function widget( selection ){
        var value = selection.append('div')
            .classed('value', true )
            .attr('id', _id )
            .attr('wide', _wide )
            .attr('type', _type )
            .property('value', _default )
            .on('tx', txHandle )
            .on('rx', rxHandle )
            .on('click', () => { d3.event.cancelBubble = true; } );


        value.append('div')
            .attr('id', 'label')
            .text( () => { if(_label) return _label + ":"; } );
        value.append('div')
            .attr('id', 'value')
            .on('keydown', keydownHandle )
            .attr('contenteditable', _edit)
            .attr( 'prefix', _prefix )
            .attr( 'suffix', _suffix )
            .text(_default);
    }

    function keydownHandle(){
        if( d3.event.which == 13 ) {
            d3.select(d3.event.target.parentNode).attr('state', 'changed');
            d3.selectAll(`#${_id}`).dispatch('tx',
                { detail: {value: d3.event.target.innerHTML } } );

            d3.event.preventDefault();
        }
        if( d3.event.which == 27 ){
            d3.select(d3.event.target.parentNode).attr('state', null);
        }
    }

    function rxHandle(){
        let widget = d3.select(this);
        if( 'time' in d3.event.detail )
            widget.attr('time', d3.event.detail.time );
        if( 'state' in d3.event.detail )
            widget.attr('state', d3.event.detail.state );
        if( 'value' in d3.event.detail )
            widget.select('*:nth-child(2)').text( `${d3.event.detail.value}` );
    }
    function txHandle() {
        let widget = d3.select(this);
        console.log('default txhandle');
    };

    // Get/Set functions
    widget.id      = function(v) { if(! arguments.length )return _id;      _id      = v; return widget; }
    widget.label   = function(v) { if(! arguments.length )return _label;   _label   = v; return widget; }
    widget.type    = function(v) { if(! arguments.length )return _type;    _type    = v; return widget; }
    widget.prefix  = function(v) { if(! arguments.length )return _prefix;  _prefix  = v; return widget; }
    widget.suffix  = function(v) { if(! arguments.length )return _suffix;  _suffix  = v; return widget; }
    widget.default = function(v) { if(! arguments.length )return _default; _default = v; return widget; }
    widget.wide    = function(v) { if(! arguments.length )return _wide;    _wide    = v; return widget; }
    widget.edit    = function(v) { if(! arguments.length )return _edit;    _edit    = v; return widget; }
    widget.txHandle= function(v) { if(! arguments.length )return _txHandle;_txHandle= v; return widget; }

    return widget;
}

/* Options
 * ------- */
function options( id = ''){
    var _id = id,
        _label,
        _options = ['null'],
        _selection = [false],
        _uniq = true,
        _style;

    function widget( selection ){
        var widget = selection.append('div')
            .attr('id', _id)
            .classed( 'options', true )
            .on('rx', rxHandle );

        widget.append('div')
            .text(_label);

        var list = widget.append('div')
            .attr('id', 'list');

        var items = list.selectAll('span')
            .data( _options );

        items.enter()
          .append('span')
            .on('click', onClick )
          .merge(items)
            .text( (d) => {return d ? d : '-';} )
            .call( (d) => {
                d.filter( (d,i) => _selection[i] )
                    .classed('selected', true );
            });
    }

    function updateIndex( widget, i ){
        if( _uniq ) _selection.fill(false);
        _selection[i] = !_selection[i];

        widget.select('#list').selectAll('span')
            .attr('selected', (d,i) => _selection[i] ? true : null );
    }

    function onClick( d,i,l ){
        var widget = d3.select(this.parentNode.parentNode );

        updateIndex( widget, i );

        widget
            .attr('state', 'changed')
            .property('value', d)
            .dispatch('tx',{ detail:{ d:d, i:i, time:Date.now(), selection:_selection, options:_options } } );
    }

    function rxHandle() {
        var widget = d3.select( this );

        if( 'index' in d3.event.detail ){
            var i = d3.event.detail.index;
            updateIndex( widget, i );
        }
        if( 'bitmask' in d3.event.detail ){
            var array = d3.event.detail.bitmask.toString(2).slice();
            for(var i = 0; i < _selection.length; ++i)_selection[i] = array[i] ? true : false;
            widget.select('#list').selectAll('span')
                .attr('selected', (d,i) => _selection[i] ? true : null );

        }
        widget
            .attr('state', 'driven')
            .property('options', _options)
            .property('selection', _selection);
    }

    widget.id    = function(v) { if(! arguments.length )return _id;    _id     = v; return widget; }
    widget.label = function(v) { if(! arguments.length )return _label; _label  = v; return widget; }
    widget.uniq  = function(v) { if(! arguments.length )return _uniq;  _uniq   = v; return widget; }

    widget.options = function(v) {
        if(! arguments.length )return _options;
        _options = v;
        _selection.length = _options.length;
        return widget; }

    return widget;
}

/* List wiget
 * the list is a little interesting in that it contains three elements which
 * all need to be customisable.
 * - the list items
 * - the control area
 * - the item properties 
 */
function list( id = '' ){
    var _id = id,
        _label,
        _data,
        _height = 100;
    var _listFunc = listFunc,
        _propFunc = propFunc,
        _ctrlFunc = ctrlFunc;

    function widget( selection ){
        widget = selection.append('div')
            .classed('list', true )
            .attr( 'id', _id )
            .on('rx', rxHandle );
        widget.append('h1').text(_label);
        let row = widget.append('div').attr('id', 'row' );
        let props = widget.append('div').attr('id', 'prop');


        let listbox = row.append('div').attr('id','listbox');
        let control = row.append('div').attr('id', 'control');
        _ctrlFunc( control );

        let list = listbox.append('div').attr('id', 'list');

        listbox.append('div').attr('id', 'filter-top').text('=')
            .on('click', function() {
                filter.classed('hide', !filter.classed('hide') );
        });
        let filter = listbox.append('div')
            .attr('id', 'filter')
            .attr('contenteditable', true )
            .text('filter box')
            .on('input', function(){
                var input = this;
                list.selectAll('*').each( function(){
                    d3.select(this).classed('hide', !this.textContent.includes( input.textContent ) );
                });
            } );;

        if( _data )_listFunc( list, _data );
    }

    //receive event handler
    function rxHandle() {
        if( 'data' in d3.event.detail ){
            _data = d3.event.detail.data;
            _listFunc( d3.select(this).select('#list'), _data );
        }
        if( 'select' in d3.event.detail ){
            d3.select(this).select('#list').selectAll('div')
              .filter( (d,i) => d3.event.detail.select == i )
                .classed('selected', true );
        }
    }

    // Default List and Prop functions
    function listFunc( selection, data ){
        let items = selection.selectAll('div').data( data );

        items.enter()
          .append('div')
            .merge(items)
          .text( (d,i) => { return `${i}:${d}`; } )
        items.exit().remove();
    }

    function propFunc( selection ){
        console.log('default properties function');
        // we need to know what the current selection is to make this work.
    }

    function ctrlFunc( selection ){
        console.log('default control function');
        selection.append('button').text('+');
        selection.append('button').text('-');
        // we need to know what the current selection is to make this work.
    }

    widget.id       = function(v) { if(! arguments.length )return _id;        _id       = v; return widget; }
    widget.label    = function(v) { if(! arguments.length )return _label;     _label    = v; return widget; }
    widget.data     = function(v) { if(! arguments.length )return _data;      _data     = v; return widget; }
    widget.listFunc = function(v) { if(! arguments.length )return _listFunc;  _listFunc = v; return widget; }
    widget.propFunc = function(v) { if(! arguments.length )return _propFunc;  _propFunc = v; return widget; }
    widget.ctrlFunc = function(v) { if(! arguments.length )return _ctrlFunc;  _ctrlFunc = v; return widget; }

    return widget;
}
