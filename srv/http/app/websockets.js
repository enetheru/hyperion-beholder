/* This module sets up the functions to perform websocket connections and
 * send/receive messages.
 * */
var socket = null;

export function connect( address ){
    socket = new WebSocket( address );
    return socket;
}

export function getSocket(){
    return socket;
}

/* handler registration functions
 * ------------------------------ */
//export function register_message_handler( function ){
//};
//
///* Websocket Control Functions
// * --------------------------- */
//export function connect( address ){
//    if( ws ){
//        return;
//    }
//
//    ws = new WebSocket( address );
//
//    document.getElementById('ws_status').innerHTML = "Connecting...";
//
//    ws.onopen = function(event){
//        document.getElementById('ws_status').innerHTML = "Connection Open";
//        ws_send(`{"path":"system/uptime","action":"get"}`);
//        ws_send(`{"path":"sd/logs",    "action":"get"}`);
//        ws_send(`{"path":"sd/info",    "action":"get"}`);
//        ws_send(`{"path":"net/info",   "action":"get"}`);
//    };
//
//    ws.onerror = function( event ){
//        document.getElementById('ws_status').innerHTML = "Connection Error";
//    };
//
//    ws.onclose =  function( event ){
//        ws = undefined;
//        document.getElementById('ws_status').innerHTML = "Connection Closed";
//    };
//
//    ws.onmessage = ws_onmessage;
//}
//
//export function disconnect(){
//    if( ws ){
//        ws.close();
//        ws = undefined;
//    }
//    else {
//        document.getElementById('ws_status').innerHTML = 'disconnected';
//    }
//}
//
///* IO
// * -- */
//export function send( msg ){
//    if( ws != undefined && ws.readyState == ws.OPEN ){
//        ws.send( msg );
//    }
//}
//
//export function send16(){
//    // function to send a packet that has a size larger than 7bit length, and
//    // smaller than 64bit length
//    send( Array(128).join('x') );
//}
//
//export function send64(){
//    // function to send a packet that has a size of at least a 64 bit length
//    send( Array(65537).join('x') );
//}
