TODO List
=========
 * make pretty, styled in an easy fashion
 * shrink code size if at all possible
 * auto scaling y dimension depending on the incoming data
 * add left hand side slide out menu for modifying graph settings
 * Stop animations if there is no data, to reduce CPU load. chromium is
 * taking 40% of one of my cpu's currently rendering the graph transitions.
 * Make Period detatched from graph width, such that it is defined as
   period per pixels or period per unit width.
 * Implement function to pause the transitions to save cpu/power
 * [DONE] resize without scaling text
 * [DONE] self contained, turned into a class, or an object
 * [DONE] make parent div movable/dragable and scalable.

Thoughts on Visual Style
------------------------
I want to pack as much data into the graph as possible, and also make it really
pretty, things to consider include, animations, both informative and
superflous, colour schemes, interactivity, fonts and lines, and such..

Data Plot Thoughts
------------------
 * line method, aka bezier, linear, spline, etc.
 * creation and deletion of line segments as fade in out, sparkly etc.
 * the movement of the line,
 * the line itself, thickness as another datapoint?, error bars as more
 * thickness?
 * multiple plots on the same graph
 * keep track of bounds and averages and visualise them
 * how much extra history to keep to calculate additional visualisation?

Line drawing Concept
--------------------
When creating and deleting line segments at the beginning and end of the graph
timeline I want the line to draw itself dynamically and slide across the screen
smoothly. The new points can pop into existence, and the old points pop out of
existence, but the visual appeal should be that it is like watching something
come into existence, bonus points for pretty animations on the creation of new
points.

Axis
----
 * TODO moving axis entry and exit.
 * TODO clip to borders, it pokes out at the moment.
 * TODO Adjust margins to accomodate.
 * TODO number format
 * TODO number of tick marks

Axis entry and exit concept
---------------------------
Having the edges fade in through the use of a gradient and fade out the same,
would stop the popping of the tick mark and the sudden popping in of the
timestamp, instead of using a clip rectangle.

Legend
------
 * TODO automatically adjust margins to fit
 * TODO Position within graph
 * TODO auto udpating values

Titles
------
 * TODO automatically adjust margins to match title size

Caption
------
 * TODO automatically adjust margins to match title size

Backgrounds
-----------
 * TODO images
 * TODO colours
 * TODO patterns
 * TODO grid, with appropriate primary and secondary lines
 * TODO arbitrary div, like a visualisation or something with d3.js
 * TODO scrolling, auto generated sidescroller platform? lol

Interactivity
=============
moving the mouse over the image is another opportunity to show more contextual
information either through labels, or by altering properties of the graph
itself, like timing, pausing the progression of time,

* TODO tooltip labels
* TODO pausing the progression of time to better inspect
* TODO controls to change graph properties

Control Panel Concept
---------------------
as the data is moving from right to left, i want to maintain a hard edge on the
right hand side that doesnt change, so any controls would need to come from the
left hand side, I'm thinking a slide out panel that effects the margins of the
graph, pushing it to the side a little. might be able to put quite a lot in
there like the typical android menu, but with actual controls to change.

List of things that are just values that can be changed
* TODO period - the timeframe that the graph is looking at
* TODO the stylesheet to use
* TODO what the tick format should be, and how many

Thought: enter and exit
-----------------------
whenever something enters and exits the visual field we have an opportunity for
something pretty, whether thats a fade in or out, or a pop, some sparks, i'm
fond of the virtual reality entrance and exit from Rick and Morty S01E04,
starting out with polygons then smoothing over. I also like the 80's
electricity with flashing pops sort of how lightning works. and the spark burn
and slow fade of hot metal as it cools. will add more as I remember. I'm also a
fan of general chrome shine bling when a mouse moves over  buttons or items, as
if we were polishing them with the mouse cursor.

On Resizing the graph
=====================
In order for the graph to display nicely at different sizes I cannot use the
viewBox attributes as it scales the svg, instead i need to rebuild the elements
with the new dimensions. this mostly involves re-making most of the graph
unfortunately.
