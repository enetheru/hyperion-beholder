import "../lib/d3-array.js"
import "../lib/d3-axis.js"
import "../lib/d3-color.js"
import "../lib/d3-dispatch.js"
import "../lib/d3-ease.js"
import "../lib/d3-format.js"
import "../lib/d3-interpolate.js"
import "../lib/d3-path.js"
import "../lib/d3-scale.js"
import "../lib/d3-selection.js"
import "../lib/d3-shape.js"
import "../lib/d3-time-format.js"
import "../lib/d3-time.js"
import "../lib/d3-timer.js"
import "../lib/d3-transition.js"


export function livegraph() {
    //timing
    var period = 60000, // timeframe represented by graph t-period;
        trn_n = 100, // transition splits
        trn_freq = period / trn_n, // split the period into n slices
        trn_dist; // needs updating from the inner rect

    // any variables that are set later and are just words for now.
    var chartSelection,
        data;

    //dimensions
    var margins = { top: 20, right: 20, bottom: 25, left: 40 };
    //FIXME base the magins on the text size
    var width = "100%", height = "100%";

    // d3 helper objects
    var xScale, yScale,
        line;

    var yDomain = [0,10];

    // all the elements are re-used across functions
    var svg,
        inner_group,
        x_axes, y_axes;


    // Reactionary observer
    var observer;

    function chart( selection ){
        /* Calculating our support data
         * ============================ */
        /* FIXME the svg element has a default minimum height of 150, which
           when created here increases the size of the div parent element. if i
           were to move this below in the svg creation part, then it would fail
           with a negaive height due to 0 height div minus margins.*/
        /* FIXME When the object is resized the height can also become negative
           due to margins, so somehow a minimum height needs to be observed */
        svg = selection.append("svg");
        svg.node().setAttribute("width", width );
        svg.node().setAttribute("height", height );

        //attach the data.
        data = selection.datum();
        chartSelection = selection;

        // get the real size on screen using the parent element
        var outer_bounds = {
            width: selection.node().offsetWidth,
            height: Math.max( selection.node().offsetHeight, height )
        }

        // now we have the outer bounds we can calculate the inner bounds
        var inner_bounds = {
            x: margins.left,
            y: margins.top,
            width:  outer_bounds.width - margins.left - margins.right,
            height: outer_bounds.height - margins.top - margins.bottom
        }

        // using the now known dimensions we can calculate other elements
        trn_dist = inner_bounds.width / trn_n;

        // Generate X Axis scale
        xScale = d3.scaleTime()
            .domain( [Date.now() - period, Date.now() + trn_freq ] )
            .range( [0,inner_bounds.width + trn_dist] );
        /* TODO I had a thought to make the range the maximum and minimum of
         * the data available.  but scrolling would be weird perhaps?*/

        //Generate Y Axis scale
        yScale = d3.scaleLinear()
            .domain( yDomain )
            .range([inner_bounds.height, 0]);

        // the function of drawing a line which takes the timestamp from the data and
        // maps it to the x dimension and the value of the data to map to the y
        // dimension
        line = d3.line()
            .curve(d3.curveBasis)
            .x( function( d, i ) { return xScale( d.time ); } )
            .y( function( d, i ) { return yScale( d.data ); } );

        /* Building the SVG Graph
         * ====================== */
        var g = svg.append("g");

        // background of the outer area
        var outer_bg = g.append("rect")
            .attr("width", "100%")
            .attr("height", "100%")
            .style("fill", "grey");

        // inner group for everything relative to the inner area all sub elements
        //no longer have to translate to the location as they are subordinate to
        //the inner_g
        inner_group = g.append("g")
            .attr("transform", "translate(" + margins.left + "," + margins.top +")");

        // background of the inner area
        var inner_bg = inner_group.append("rect")
            .attr("width", inner_bounds.width )
            .attr("height", inner_bounds.height )
            .style("fill", "darkgrey");

        // Append  a group element to use for the x axis, it will befilled out as part
        // of the transition
        x_axes = inner_group.append("g")
          .transition()
            .duration(trn_freq)
            .ease( d3.easeLinear )
            .on("start", tick_axis );

        // add the y axis
        y_axes = inner_group.append("g")
            .call( d3.axisLeft( yScale ) );

        // append a defs section with a rect to use as a clipping rectangle so lines
        // lines outside the inner rectangle are cut off.
        inner_group.append("defs")
          .append("clipPath")
            .attr("id", "inner_clippath")
          .append("rect")
            .attr("width", inner_bounds.width )
            .attr("height", inner_bounds.height );

        //now we have our area to draw our line
        var graph_g = inner_group.append("g")
            .attr("clip-path", "url(#inner_clippath)"); //attach the clip path.

        // add to the graph group the line
        graph_g.append("path")
            .datum(data)
            .attr("d", line)
            .attr("fill", "none")
            .attr("stroke", "#000")
            .attr("stroke-width", "1px")
          .transition()
            .duration( trn_freq )
            .ease( d3.easeLinear )
            .on("start", tick );


        /* Setup the parent observer incase it changes, we will adjust
         * -----------------------------------------------------------*/
        var observer_config = { attributes: true };
        observer = new MutationObserver(
            function( mutationlist, observer ){
                for( var mutation of mutationlist ){
                    if( mutation.type == 'attributes'){
                        var width = d3.select("#graph")
                            .node().offsetWidth;
                        var height = d3.select("#graph")
                            .node().offsetHeight;

                        chart.updateWidth( width ).updateHeight( height );

                    }
                }
            });

        observer.observe( selection.node(), observer_config );

        /* Finished Setup
         * -------------- */
    }

    function tick_axis() {
        //update the xScale with the new domain
        xScale.domain([Date.now() - period, Date.now() ]);

        //update the x axis
        //reset the position of the x axis.
        d3.select(this)
            .attr("transform", "translate(0," + yScale(0) + ")")
            .call( d3.axisBottom( xScale )
                    .tickFormat( d3.timeFormat("%a-%H:%S") )
            );

        // active returns the current transition, and then chains a new one to run
        // after it finishes. transitions are relative to each other so the initial
        // frequency is propogated unless changed.
        d3.active(this)
            .attr("transform", "translate( -" + trn_dist + "," + yScale(0) + " )" )
          .transition()
            .on("start", tick_axis );
    }

    function tick() {
        //Update the line data and reset the position
        d3.select( this )
            .attr("d", line )
            .attr("transform", null );

        var data = d3.select( this ).datum();
        while( true ){
            //If we run out of data, shift the last entry to the right so that
            //new data will continue it brand new rather than have a long weird
            //tail.
            if( data.length <= 1 ){
                data[0]['time'] = Date.now();
                break;
            }
            //FIXME this will delete all the data in the case that there
            //is no incoming, it was put here so that the horizontal size
            //ofthe graph didnt explode over time. I would rather the graph
            //stop transitioning on no incoming data.
            if( data[0]["time"] < Date.now() - period ){
                data.shift();
            } else {
                break;
            }

        }

        d3.active(this)
            .attr('transform', `translate( -${trn_dist}, 0)`)
          .transition()
            .on("start", tick );
    }


/* Alter Starting Properties
 * ------------------------- */
    chart.width = chartWidth;
    chart.height = chartHeight;
    chart.yDomain = chartYDomain;
    chart.period = chartPeriod;

    function chartWidth( value ) {
        if(! arguments.length) { return width; }
        width = value;

        return chart;
    }

    function chartHeight( value ){
        if(! arguments.length) { return height; }
        height = value;

        return chart;
    }

    function chartYDomain( value ){
        if(! arguments.length ){ return yDomain; }
        yDomain = value;

        return chart;
    }

    function chartPeriod( value ){
        if(! arguments.length ){ return period; }
        period = value;
        trn_freq = period / trn_n;

        return chart;
    }

/* Alter Live Properties
 * --------------------- */
    chart.updateWidth = updateWidth;
    chart.updateHeight = updateHeight;

    function updateWidth( value ) {
        if(! chartSelection )return chart;
        width = value;
        var inner_width = width - margins.left - margins.right;

        var svg = chartSelection.select("svg");
        svg.node().setAttribute("width", width );

        svg.select("g").select("rect").attr("width", width )
        inner_group.select("rect").attr("width", inner_width )
        inner_group.select("defs").select("clipPath").select("rect")
            .attr("width", inner_width );


        trn_dist = inner_width / trn_n;
        xScale.range([0, inner_width + trn_dist]);


        return chart;
    }

    function updateHeight( value ){
        if(! chartSelection )return chart;
        height = value;
        var inner_height = height - margins.top - margins.bottom;

        var svg = chartSelection.select("svg");
        svg.node().setAttribute( "height", height );

        svg.select("g").select("rect").attr("height", height );
        inner_group.select("rect").attr("height", inner_height );
        inner_group.select("defs").select("clippath").select("rect")
            .attr("height", inner_height );

        yScale.range([inner_height, 0]);

        y_axes.call( d3.axisLeft( yScale ) );


        return chart;
    }

    return chart;
}

