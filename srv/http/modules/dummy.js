import * as wj from '/app/widgets.js';

export { init, cb_websocket };

function init( selection ){

    var titlebar = selection.select('#titlebar');
    titlebar.select('h1').text('Dummy Module');

    var content = selection.select('#content');
    content.call( wj.section('dummy-section').title('Section Widget').depth(2) );
    content.call( wj.value().label('Value Widget (type=default)') );
    content.call( wj.value().label('Value Widget (type=text)').type('text') );
    content.call( wj.value().label('Value Widget (type=integer)').type('integer') );

    content.call( wj.options()
        .label('Options Widget (uniq=true)')
        .options(['QUIET','FATAL', 'ERROR', 'WARNING','INFO','DEBUG','OVERWHELM','ZION']) );
    content.call( wj.options()
        .label('Options Widget (uniq=false)')
        .uniq(false)
        .options([,,,,,,,'test',,,,,]) );

    content.select('#dummy-section > #content')
      .append('div')
        .text('Dummy Section Contents');

    return true;
}

function cb_websocket( event ){
    var register = [];

    function register_ob( value ){
        if(! arguments.length) { return register; }
        register.push( value );
    }
}
