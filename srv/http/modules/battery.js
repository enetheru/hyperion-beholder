import * as graph from '/app/livegraph.js';
import * as wj from '/app/widgets.js';
export { init, cb_websocket };

/* Battery Module */

var volts_data = [{time:Date.now()-100000,data:0}];

function init( section ){
    let hostname = d3.select('#hostname').property('value');
    var titlebar = section.select('#titlebar');
    var content = section.select('#content');

    titlebar.call( wj.value('volts').label('Voltage').suffix(' V') );

    //FIXME make this editable
    content.call( wj.value('calibrate').label('Calibration Multiplier') );
    content.call( wj.value('raw').label('Raw Sensor Value') );

    var volts_chart = graph.livegraph()
        .width( "100%")
        .height(128)
        .yDomain( [0,15] );

    content.append('div')
        .attr('id', 'volts-chart')
        .datum( volts_data )
        .call( volts_chart );

    for( var item of ['volts', 'calibrate', 'raw']){
        fetch(`http://${hostname}/p/battery/${item}`)
        .then( r => { cb_http( r ); } );
    }
    return true;
}

function cb_websocket(){
    var msg = JSON.parse(this.data);
    //var msg = JSON.parse( d3.event.detail );

    if( msg.path == 'battery' ){
        //FIXME because the time on the device is not the sam as the
        // system time the graph displays the graph off centre,
        //sometimes outside the viewport.
        d3.selectAll('#volts').dispatch('rx', 
        { detail:{ state:'animated', value: msg.volts, time:Date.now() } } );

        volts_data.push({
            time:Date.now(),// FIXME time:Date.parse(message.datetime),
            data:msg.volts
        });
        if( volts_data.length > 2000 )volts_data.shift();
    }
}

function cb_http( r ){
    r.json().then( json => {

    let hostname = d3.select('#hostname').property('value');
    var section = d3.select('#battery');

    if( json.path == 'battery/volts' ){
        section.selectAll(`#volts`)
            .dispatch('rx', 
        { detail:{ state:'driven', value: json.value, time:Date.now() } } );
    }
    if( json.path == 'battery/calibrate' ){
        section.selectAll(`#calibrate`)
            .dispatch('rx', 
        { detail:{ state:'driven', value: json.value, time:Date.now() } } );
    }
    if( json.path == 'battery/raw' ){
        section.selectAll(`#raw`)
            .dispatch('rx', 
        { detail:{ state:'driven', value: json.value, time:Date.now() } } );
    }
    });
}
