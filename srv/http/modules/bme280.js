import * as graph from "../app/livegraph.js";
import * as wj from '/app/widgets.js';
export { init, cb_websocket };

/* BME280
 * Brainstorm for module
 * ---------------------
 *  to follow up on the existing object,  there are a numbeer of variables thta
 *  need to be tracked and perhaps grpahed.
 *
 *  - Temeraturue
 *  - Humidity
 *  - Air Pressure
 *  - calcualted Altitude based on sea level pressure in my location which will
 *  require GPS and some form of weather service query to the internet, so not
 *  suitable for realtime capture. Might be able to be correlated later in the
 *  Aggregate.
 *
 *  Given i have these attributes I basically have a barometer, which will
 *  allow me to view things, so perhaps i could ask a weather expert on what
 *  sort of things can be derived from  these attributes.
 *
 *  reading on the internet shows that low barometric pressure means storms,
 *  and high means fair weather, so using that and humidity i might be able to
 *  predict the weather outside a little.
 *
 *  So there is some research that needs to be done to get the full use of this
 *  sensor. for now we'll start with three graphs and values shown
 */


/* Data for realtime graphs */
var temp_data = [{time:Date.now()-100000,data:0}];
var pres_data = [{time:Date.now()-100000,data:0}];
var humi_data = [{time:Date.now()-100000,data:0}];

function init( section ){
    let hostname = d3.select('#hostname').property('value');
    var titlebar = section.select('#titlebar');
    var content = section.select('#content');

    titlebar.call( wj.value('temperature').label('Temp').suffix('°C') );
    titlebar.call( wj.value('humidity').label('Humidity') );
    titlebar.call( wj.value('pressure').label('Pressure') );

    let temp_chart = graph.livegraph()
        .height(128)
        .yDomain( [-15,50] );

    content.append('div')
        .attr('id', 'temp')
        .datum( temp_data )
        .call( temp_chart );

    let pres_chart = graph.livegraph()
        .width( "100%")
        .height(128)
        .yDomain( [0,1000] );
    content.append('div')
        .attr('id', 'pres')
        .datum( pres_data )
        .call( pres_chart );

    let humi_chart = graph.livegraph()
        .width( "100%")
        .height(128)
        .yDomain( [0,100] );
    content.append('div')
        .attr('id', 'humi')
        .datum( humi_data )
        .call( humi_chart );

    for( var item of ['config', 'status']){
        fetch(`http://${hostname}/p/bme280/${item}`)
        .then( r => { cb_http( r ); } );
    }
    return true;
}

function cb_websocket( evt ){
    let json = JSON.parse( this.data );
    if(! json.path.startsWith('bme280') )return;

    let section = d3.select('#bme280');

    section.select('#temperature').dispatch('rx', 
        { detail:{ state:'animated', time:Date.now(), value: json.temp } } );

    section.select('#humidity').dispatch('rx', 
        { detail:{ state:'animated', time:Date.now(), value: json.humi } } );

    section.select('#pressure').dispatch('rx', 
        { detail:{ state:'animated', time:Date.now(), value: json.pres } } );

    section.select('#altitude').dispatch('rx', 
        { detail:{ state:'animated', time:Date.now(), value: json.alti } } );

    //FIXME because the time on the device is not the sam as the
    // system time the graph displays the graph off centre,
    //sometimes outside the viewport.
    temp_data.push({
        time: Date.now(),// FIXME time:Date.parse(message.datetime),
        data: json.temp
    });
    if( temp_data.length > 2000 )temp_data.shift();

    pres_data.push({
        time: Date.now(),// FIXME time:Date.parse(message.datetime),
        data: json.pres
    });
    if( pres_data.length > 2000 )pres_data.shift();

    humi_data.push({
        time: Date.now(),// FIXME time:Date.parse(message.datetime),
        data: json.humi
    });
    if( humi_data.length > 2000 )humi_data.shift();
}

function cb_http( r ){
    r.json().then( json => {

    let hostname = d3.select('#hostname').property('value');
    let section = d3.select('#bme280');

    if( json.path == 'bme280/config' ){
        //TODO create more controls.
        //"rt_freq": 1000,
        //"sd_freq": 300000,
        //"slp": 0
    }
    if( json.path == 'bme280/status' ){
        section.select('#temperature').dispatch('rx', 
        { detail:{ state:'driven', time:Date.now(), value: json.temp } } );

        section.select('#humidity').dispatch('rx', 
        { detail:{ state:'driven', time:Date.now(), value: json.humi } } );

        section.select('#pressure').dispatch('rx', 
        { detail:{ state:'driven', time:Date.now(), value: json.pres } } );

        section.select('#altitude').dispatch('rx', 
        { detail:{ state:'driven', time:Date.now(), value: json.alti } } );
    }

    });
}
