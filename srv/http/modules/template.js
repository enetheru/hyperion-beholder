import * as wj from '../app/widgets.js';
export { init, cb_websocket };

function init( section ){
    let hostname = d3.select('#hostname').property('value');
    let titlebar = section.select('#titlebar');
    let content = section.select('#content');

    // Create Titlebar
    titlebar.select('h1').text('Template Widget');
    titlebar.append('button').text('Refresh')
        .on('click', () => {
            d3.event.cancelBubble = true;
            fetch(`http://${hostname}/p/system/info`)
            .then( r => { cb_http( r ); } );
        });

    //Create Content
    content.append('p').text('Template Widget Content');

    return true;
}

function cb_websocket( ){
    var msg = JSON.parse(this.data);
    var section = d3.select('#system');
}

function cb_http( r ){
    r.json().then( json => {

    let hostname = d3.select('#hostname').property('value');


    });
}
