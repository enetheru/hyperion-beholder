import * as wj from '/app/widgets.js';
export { init, cb_websocket };

/* LEDS Module
 * */


var volts_data = [{time:Date.now()-100000,data:0}];

function init( section ){
    let hostname = d3.select('#hostname').property('value');
    /* Create the title */
    var titlebar = section.select('#titlebar');
    var content = section.select('#content');

    titlebar.append('button')
        .text('On')
        .on('click', () => {
            d3.event.cancelBubble = true;
            fetch(`http://${hostname}/p/leds/set?rgb=FF9F44&brightness=255`)
            .then( r => { cb_http( r ); } );
        });
    titlebar.append('button')
        .text('Medium')
        .on('click', () => {
            d3.event.cancelBubble = true;
            fetch(`http://${hostname}/p/leds/set?rgb=53992e&brightness=32`)
            .then( r => { cb_http( r ); } );
        });

    titlebar.append('button')
        .text('Off')
        .on('click', () => {
            d3.event.cancelBubble = true;
            fetch(`http://${hostname}/p/leds/set?brightness=0`)
            .then( r => { cb_http( r ); } );
        });

    titlebar.call( wj.value('rgb').label('RGB').type('colour').prefix('#') );

    content
        .call( wj.value('rgb').label('RGB Colour Code').type('colour').prefix('#') )
        .call( wj.value('brightness').label('Brightness') )
        .call( wj.value('pin').label('Pin').type('integer').edit(false) )
        .call( wj.value('num-pixels').label('No. Pixels').edit(false) );

    content.select('#rgb').on('tx', function() {
        let value = d3.event.detail.value;
        d3.event.cancelBubble = true;
        fetch(`http://${hostname}/p/leds/set?rgb=${value}`)
        .then( r => { cb_http( r ); } );
    });
    content.select('#brightness').on('tx', function() {
        let value = d3.event.detail.value;
        d3.event.cancelBubble = true;
        fetch(`http://${hostname}/p/leds/set?brightness=${value}`)
        .then( r => { cb_http( r ); } );
    });

    //fetch values
    fetch(`http://${hostname}/p/leds/status`)
    .then( r => { cb_http( r ); } );

    fetch(`http://${hostname}/p/leds/config`)
    .then( r => { cb_http( r ); } );
    return true;
}

function cb_websocket(){
    var msg = JSON.parse(this.data);
    var section = d3.select('#leds');
}

function cb_http( r ){
    r.json().then( json => {

    let section = d3.select('#leds');

    if( json.path == 'leds/set'){
        if( 'brightness' in json ){
            section.selectAll('#brightness').dispatch('rx',
            { detail:{ state:'driven', time:Date.now(), value: json.brightness } } );
        }
        if( 'rgb' in json ){
            section.selectAll('#rgb').dispatch( 'rx',
            { detail:{ state:'driven', time:Date.now(), value: json.rgb } } );
        }
    }

    if( json.path == 'leds/status' ){
        if( 'brightness' in json ){
            section.selectAll('#brightness').dispatch('rx',
            { detail:{ state:'driven', time:Date.now(), value: json.brightness } } );
        }
        if( 'pixel-color' in json ){
            section.selectAll('#rgb').dispatch( 'rx',
            { detail:{ state:'driven', time:Date.now(), value: json['pixel-color'] } } );
        }
    }

    if( json.path == 'leds/config' ){
        if( 'pin' in json ){
            section.selectAll('#pin').dispatch('rx',
            { detail:{ state:'driven', time:Date.now(), value: json.pin } } );
        }

        if( 'num-pixels' in json ){
            section.selectAll('#num-pixels').dispatch('rx',
            { detail:{ state:'driven', time:Date.now(), value: json['num-pixels'] } } );
        }
    }

    });
}
