import * as wj from '../app/widgets.js';

export { init, cb_websocket };

var log_levels = ['QUIET','FATAL','ERROR','WARN','INFO','DEBUG','OVERWHELM','ZION'];

function booleanArrayToDecimal( source ){
    let array = Array(source.length);
    for(let i = 0; i < source.length; ++i)array[i] = source[i] ? '1' : '0';
    return parseInt( array.join(''), 2 );
}

function init( section ){
    let hostname = d3.select('#hostname').property('value');
    let titlebar = section.select('#titlebar');
    let content = section.select('#content');

    // Create Titlebar
    titlebar.call( wj.value('period_ms').label('LoopTime').suffix(' ms') );

    //Create Info Section
    content.call( wj.section('info').title('Info').depth(2) );
    content.select('#info > #titlebar').append('button').text('Refresh')
        .on('click', () => {
            d3.event.cancelBubble = true;
            fetch(`http://${hostname}/p/system/info`)
            .then( r => { cb_http( r ); } );
        });
    content.select('#info > #content')
        .call( wj.value('name').label('Name') )
        .call( wj.value('period_ms').label('LoopTime').suffix(' ms') )
        .call( wj.value('local_time').label('Local Time') )
        .call( wj.value('uptime').label('Up Time') )
        .call( wj.value('extra_info').label('Extra Info') );

    // Create Logger Section
    content.call( wj.section().title('Logger').id('logger').depth(2) );
    content.select('#logger > #titlebar').append('button').text('Refresh')
        .on('click', () => {
            d3.event.cancelBubble = true;
            fetch(`http://${hostname}/p/system/logger`)
            .then( r => { cb_http( r ); } );
        });
    var logger = content.select('#logger').select('#content');
    logger
        .call( wj.options('log_level').label('Log Level').options( log_levels ) )
        .call( wj.options('log_outputs').label('Log Outputs').uniq(false)
            .options([,,,,,,,,]) )
        .call( wj.options('log_contexts').label('Log Contexts').uniq(false)
            .options([,,,,,,,,]) );

    logger.select('#log_level')
        .on('tx', function() {
            var level = d3.event.detail.d;
            fetch(`http://${hostname}/p/system/logger?log_level=${level}`)
            .then( r => { cb_http( r ); } );
        });

    logger.select('#log_outputs')
        .on('tx', function() {
            let mask = booleanArrayToDecimal( d3.event.detail.selection );
            fetch(`http://${hostname}/p/system/logger?log_output_mask=${mask}`)
            .then( r => { cb_http( r ); } );
        });

    logger.select('#log_contexts')
        .on('tx', () => {
            let mask = booleanArrayToDecimal( d3.event.detail.selection );
            fetch(`http://${hostname}/p/system/logger?log_context_mask=${mask}`)
            .then( r => { cb_http( r ); } );
        });

    var logfiles = {};
    logfiles.listFunc = function listFunc( selection, data ){
        let items = selection.selectAll('div').data( data );

        items.enter()
          .append('div')
            .each( function(){ d3.select(this).append('a'); })
        .merge(items)
            .each( function(d,i){
                d3.select(this).select('a')
                    .attr('href', `http://${hostname}/${d}`)
                    .text( (d,i) => d );
            });

        items.exit().remove();
    }
    logger.call( wj.list('log_files').label('Log Files')
        .listFunc( logfiles.listFunc ) );

    var logbuffer = {};
    logbuffer.activeNode = null;
    logbuffer.data = Array(0);
    let stylesheet = document.styleSheets[0];
    stylesheet.insertRule(`#log_buffer #list > div {
        margin-top: var(--margins);
        margin-right: var(--margins);
        position:relative;
        border-radius: var(--roundness);
        white-space: nowrap;
        overflow: hidden;
        max-width: 100%;
    }`);
    stylesheet.insertRule(`#log_buffer #list > *:nth-of-type(odd) {
        background-color: var(--list-alternate);
    `);
    stylesheet.insertRule(`#log_buffer #list > div.selected {
        background-color: var(--value-inner);
        border: 1px solid var(--text-inner);
    `);

    stylesheet.insertRule(`#log_buffer #list span {
        display:inline-block;
        border-radius: var(--roundness);
        padding-left: var(--margins);
        padding-right: var(--margins);
        overflow:hidden;
        text-align: center;
        white-space:nowrap;
    }`);

    logbuffer.listFunc = function( selection, data ){
        let line = selection.append('div').call( (node) => {
            node.append('span').text(`${data.level}`)
                .attr('class', () => { return data.level.toLowerCase();})
                .style('width', '4em');
            node.append('span').text(`${data.datetime}`)
                .style('width', '10em');
            node.append('span').text('|');
            node.append('span').text(`${data.message}`)
                .style('text-align', 'left');
        }).on('click', function() {
            d3.event.cancelBubble = true;
            console.log('this', this);
            console.log('event', d3.event );

            if( logbuffer.activeNode != null )
                d3.select(logbuffer.activeNode).classed('selected', false);
            logbuffer.activeNode = this;
            d3.select(this).classed('selected', true);

            console.log( logbuffer.activeNode )
            console.log( logbuffer.data.indexOf( logbuffer.activeNode ) );
        });

        console.log('newnode', line.node() );
        logbuffer.data.push( line.node() );
        selection.node().scrollTo( 0, selection.node().scrollHeight );
    }
    logbuffer.ctrlFunc = function( node ){
        node.append('button').text('C')
            .on('click', function() {
                content.select('#log_buffer').select('#list')
                    .selectAll('*').remove();
                logbuffer.data = [];
            });
        node.append('button').text('P')
            .on('click', function() {
                //TODO Pause stream or disable scrolling to button
            });
    }

    logger.call( wj.list('log_buffer')
        .label('Log Buffer')
        .listFunc( logbuffer.listFunc )
        .ctrlFunc( logbuffer.ctrlFunc ) );

    //Create SD Card Section
    content.call( wj.section().title('SD Card').id('sdcard').depth(2) );
    content.select('#sdcard > #titlebar').append('button').text('Refresh')
        .on('click', () => {
            d3.event.cancelBubble = true;
            fetch(`http://${hostname}/p/system/sdcard`)
            .then( r => { cb_http( r ); } );
        });
    content.select('#sdcard').select('#content')
        .call( wj.value('enabled').label('Enabled') )
        .call( wj.value('card_type').label('Card Type') )
        .call( wj.value('card_size').label('Card Size') )
        .call( wj.value('volume_block_count').label('Volume Block Count') )
        .call( wj.value('cluster_count').label('Cluster Count') )
        .call( wj.value('blocks_per_cluster').label('Blocks per Cluster') )
        .call( wj.value('free_cluster_count').label('Free Cluster Count') )
        .call( wj.value('fat_type').label('Fat Type') )
        .call( wj.value('blocks_per_fat').label('Blocks Per Fat') )
        .call( wj.value('fat_count').label('Fat Count') );

    //Create Network Section
    content.call( wj.section().title('Network').id('network').depth(2) );
    content.select('#network > #titlebar').append('button').text('Refresh')
        .on('click', () => {
            d3.event.cancelBubble = true;
            fetch(`http://${hostname}/p/system/network`)
            .then( r => { cb_http( r ); } );
        });
    content.select('#network').select('#content')
        .call( wj.value('link_status').label('Link Status') )
        .call( wj.value('hardware_status').label('Hardware Status') )
        .call( wj.value('local_mac').label('Local MAC') )
        .call( wj.value('local_ip').label('Local IP') )
        .call( wj.value('subnet_mask').label('Subnet Mask') )
        .call( wj.value('gateway_ip').label('Gateway IP') )
        .call( wj.value('dns_server_ip').label('DNS IP') )
        .call( wj.section('sockets').title('Sockets').depth(3) );
    content.select('#sockets > #content')
        .append('div').attr('id', 'socket_list').text('unfinished');

    for( var item of ['config', 'info', 'sdcard', 'logger', 'network']){
        fetch(`http://${hostname}/p/system/${item}`)
        .then( r => { cb_http( r ); } );
    }
    return true;
}

function cb_websocket( ){
    var msg = JSON.parse(this.data);
    var section = d3.select('#system');

    if( msg.path == 'system' ){
        section.selectAll('#period_ms').dispatch('rx',
        { detail:{ state:'animated', value: msg.period_ms, time:Date.now() } } );

        section.selectAll('#sockets').dispatch('rx',
        { detail:{ state:'animated', value: msg.sockets, time:Date.now() } } );

        var items = section.select('#sockets > #content')
            .selectAll('div').data( msg.sockets );

        items.enter()
            .append('div')
            .classed('output-value', true )
            .classed('success', true )
          .merge(items)
            .text( (d) => { return d; } );
    }



    if( msg.path == 'log/syslog' ){
        section.select('#logger')
            .select('#log_buffer').dispatch('rx',
                { detail:{ data: msg } } );
    }
}

function cb_http( r ){
    r.json().then( json => {

    let hostname = d3.select('#hostname').property('value');
    var section, keys = [];

    if( json.path == 'system/info' ){
        section = d3.select('#system').select('#info');;
        keys = ['name', 'uptime'];
    }
    if( json.path == 'system/sdcard' ){
        section = d3.select('#system').select('#sdcard');;
        keys = [ "enabled",
                 "card_type",
                 "card_size",
                 "volume_block_count",
                 "cluster_count",
                 "blocks_per_cluster",
                 "free_cluster_count",
                 "fat_type",
                 "blocks_per_fat",
                 "fat_count"];
    }
    if( json.path == 'system/logger' ){
        section = d3.select('#system').select('#logger');

        var index = log_levels.findIndex( (e) => e == json.log_level );

        section.select('#log_level').dispatch('rx', 
        { detail:{ state:'driven', time:Date.now(), index: index } } );

        section.select('#log_outputs').dispatch('rx', 
        { detail:{ state:'driven', time:Date.now(), bitmask: json.log_output_mask } } );

        section.select('#log_contexts').dispatch('rx', 
        { detail:{ state:'driven', time:Date.now(), bitmask: json.log_context_mask } } );

        keys = [ "log_contexts",
                 "log_outputs" ];

        var log_files = d3.select( '#log_files' ).dispatch('rx',
            { detail: {data: json.log_files} } );
    }
    if( json.path == 'system/network' ){
        section = d3.select('#system').select('#network');;
        keys = [ "link_status",
                 "hardware_status",
                 "local_mac",
                 "local_ip",
                 "subnet_mask",
                 "gateway_ip",
                 "dns_server_ip" ];
    }


    for( var key of keys){
        section.select(`#${key}`)
            .dispatch('rx', 
        { detail:{ state:'driven', value: json[key], time:Date.now() } } );
    }


    });
}

function sd_toggle( obj ){
    // 'TODO: implement SD card enable/disable
}
