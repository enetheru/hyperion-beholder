import * as wj from '/app/widgets.js';
export { init, cb_websocket };

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

var list_data = ['one', 'two', 'three'];

function init( selection ){
    let hostname = d3.select('#hostname').property('value');
    let titlebar = selection.select('#titlebar');
    let content = selection.select('#content');

    titlebar.select('h1').text('List Testing Module');
    titlebar.append('button').text('refresh').on('click', function() {
        d3.event.cancelBubble = true;
        let data = Array( getRandomInt(1,10) );
        content.select('#list_plain').dispatch('rx',
            {detail: {data: data } } );
        });

    //custom list drawing function
    var listFunc = function(selection, data){

        selection.selectAll('div').remove();
        let items = selection.selectAll('div').data( data );
        //deletion

        //creation
        items.enter()
            .each( function( d,i,o ){
                d3.select(this).call(
                    wj.value(`item${i}`).label(`Item ${i}`).default(i) );
            })
    }

 //   content.call( wj.list('list_plain')
 //       .label('List Widget')
 //       .data(list_data)
 //       .listFunc( listFunc ) );



    //buffer type object custom drawing function
    var buffer_data = [];
    var bufferlistFunc = function( selection, data ){
        var line =  selection.append('div').text(data);
        buffer_data.push( line );
    }
    var ctrlFunc = function( node ){
        node.append('button').text('+')
            .on('click', function() {
            content.select('#list_buffer').dispatch( 'rx',
                { detail: {data: "I must do my homework" } } );
            });
        node.append('button').text('C')
            .on('click', function() {
                content.select('#list_buffer').select('#list')
                    .selectAll('*').remove();
                buffer_data = [];
            });
    }

    content.call( wj.list('list_buffer')
        .label('bufferList Widget')
        .listFunc( bufferlistFunc )
        .ctrlFunc( ctrlFunc ) );
    return true;
}

function cb_websocket( ){ }

function cb_http(){ }
