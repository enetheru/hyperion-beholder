Thoughts
========

ok, so i have both reading and writing through websockets working, The entire
point of this was because some things need adjusting on the fly, and some
things like lights and such can be turned on and off.

So i'm at the stage of writing a really simple control interface in html, and
sending control events to the arduino to deserialise and react to.

with the current setup these are some things i can adjust
* voltage sensor calibration
* time
* loop frequencies
* SD card functionality toggle
*

it will be easy on the C++ side of things, the web interface is something new
to me.

so I need a button basically, that sends a json string.

i kind of think about the fact that perhaps the frequency control should be per
sensor, i mean it doesnt reall matter if there are specific frequencies per
sensor and it makes the frequencies taylored to the thing it is trying to
monitor. it might also sort of create a structure where you build a monitor
class and sort of register it with the service, the interface would have
elements that hook into the code. you build the c++ and the interface and then
 that becomes the module you would import into the specific board you want to
install. man i need a shower, i wonder if there is a goodlife gym around here
somewhere.


Thought
-------
one thing that might be useful is for the website that it serves check the time
from the internet somehow and then push that using the websockets to the
arduino. that way when it comes to daylight savings so long as i access the
device it will update itself properly.

I can also optionally switch it to utc or local time or i might report both the
utc time and the local offset based on my world location.
i think thats a pretty handy feature of a website that it could do that, offset
the logic to the connecting device.

but of course get it to do as much processing locally as it can.

another thought
---------------
I need to treat the various arduino's around the van as if they were on a
separate bus, but i still want to use ethernet for that bus, so a second
channel of communication, i think based on a VLAN would be the best way to get
a separation between regular network traffic that has exposure to the outside
world, and the inside networks that pass messages between the devices.

then its both the individual items and the router that needs to be compromised,
or the physical devices themselves.
this greatly reduces the attack surface i think.

http GET/PUT/DELETE
-------------------
having thoughts about how to deploy code changes to a device, and what i think
is that if i implement the http methods PUT and DELETE i can wipe any static
data from the SD card, or put anything up there i like, can even create scripts
that pull config, and open vim, then push back once its done. so this way http
and interface changes can make it up to the device easily, c++ code will be
much different because if I need to add periphery then there needs to be
physical access to the device anyway, so doesnt matter that i will have to
re-flash.


Thu 21 Feb 2019 13:56:06 ACDT
-----------------------------
Thinking about how the file structure of this project doesnt really inspire me
to understand whats going on i think i should re-do it.

for instance, today i wanted to work on the website for the aggregate, but i
was sort of blocked because i was working in some separate directory from the
beholder where most of my code exists, and its sort of spread out.. so instead
of grouping things by their physical separation, i'm thinking of grouping them
by their code separation liek this

/hyperion
    /firmware
        /ethermega
            /beholder
        /rpi
            /beholder
            /aggregate
    /http
        /beholder
        /aggregate

the problem i guess is that i am thinking in terms of static web pages still..
the websocket firmware is very much a part of the project. so perhaps i can
turn myself around and think about it differently.

if i go the the device route this is perhaps what it would look like

/hyperion
    /rpi
        /aggregate
            /http
            /firmware
    /ethermega
        /beholder
            /firmware
            /http
    /aws
        /aggregate
            /http
            /services

I'm not to keen on that either.. perhaps lets try to focus on the components
and see again what that looks like in comaprison

/hyperion
    /beholder
        /atmega2560-firmware
            /modules
        /http
            /lib
            /modules
            /index.html
        /x64-server
        /x86-server
    /aggregate
        /x86-server
        /http
    /periphery

OK so given that i cant re-arrange it to be a simple structure for http
development, i think that perhaps my initial idea that my local pc has a
hyperion folder, since there is no 'hyperion' website but instead hyperion is the
system through which the aggregates and beholders display their inforamtion.

so rather than have a hyperion folder in my laptop's http directory that
spefically displays a site, instead what i will have is a beholder and an
aggregate folder that does the separate tasks, and honestly that makes more
sense now.

time to get cracking.


ok so i found out that in order for nginx to be able to access the files the
whole file tree needs to have the correct permissions, not just some folder
buried deep.. so how does git play backwards like that? it doesnt..

so perhaps i will have to return to sub repo's for http content, have the
entire repo pulled into the web host? bleh..

hmm, maybe i need to rethink my git strategy, create an srv directory and host
everything under that.

Fri 22 Feb 2019 20:01:39 ACDT
-----------------------------
I have found out a couple of extra things, i think i need to make a todo file
for this project to track the high level things.

basically d3.js as of version 4 is a collection of smaller libraries, so it
should allow me to selectively pull only what i need into it. that means that
the low powered webserver i built for the arduino can save some time..  ok lets
create a todo.

there was something else too i cant quite put my finger on again, oh yeah
creating a server for a regular linux computer so that it can push data to the
nginx websocket proxy

Wed 15 May 2019 10:08:27 ACST
-----------------------------
Found a way to get back into this project after a month or two haitus, just
read the source code and documentation, there are enough simple errors in there
and  stupid shit that i immetiately have things to do. its pretty hilarious.

I have also found that the structure of the documentation is wanting, i think
this is primarily due to the fact that the scope is quite large, and as the
project is still very immature some things have not yet become apparent to me
which would help with separation of concernes, for instance; there are multiple
subprojects within this peoject that could be stand alone
* SD card access library for multiple files
* lumberjack logging facility
* http/websocket server
* configuration loader

and each module could be considered a separate projct eventually i guess but
they will be more cookie cutter by design.

So i have found a bunch of shit in my code and there are opportunities a plenty
to inprove things, but its a matter of whether i do something newish, like the
periphery registry? or whether i keep reading and double check all the TODO
items for accuracy,. i think the latter.

also the project is getting fairly large, it might be nice to have smaller test
projects which use the libraries but only provide the simplest test bed for
checking code.

Wed 15 May 2019 13:02:53 ACST
-----------------------------
there is so much to do.. and some fairly large items in the list that are
required. like round robin network requests.. i need to create a queue or
something and process each request in parts till they are finished.

it wouldnt be a queue though, more like a simple list, and a way to cancel the
request, it will slow down processing but thats ok, i can send little chunks at
a time to the clients, thats one messy job i dont relish the idea of making.

So far I have identified to major projects that needs work
* round robin network communication to serve multiple clients at the same time
  without blocking
* periphery registry to allow multiple sensors to easily communicate back to
  the host.

Interestingly i can setup the beholder as its own periphery, providing its own
control interface, sort of eating its own dog food. i think thats the project i
want to work on first

GSat 18 May 2019 15:22:56 ACST
-----------------------------
now i have something that almost works i ahev a problem with the way i have
been sending the json strings, so i have to figure out something new.

each periphery might end up having multiple properties, so in order to
accomdate i will create something like this

```json
{
    "datetime":"timestamp",
    "periphery":"name",
    "data": {
        "key":"value",
        "key":"value",
        "key":"value"
    }
}
```

this means that when i am querying the object i can almost use the same thing i
have right now.

Tue 21 May 2019 09:58:34 ACST
-----------------------------
Finally have some valuable progress on my arduino project. the LED strip is
controllable, which means for any simple digital, or analogue circuit i am
golden.. just extend the peripheral base class and it just does the thing.

I do want to get POST messages working, but i think i should pivot to the
interface now.

For th current iteration the cmmand messages look very much like the realtime
ones

```json
{
    "type":"command"
    "name":<periphery>
    "command":<command>
    "data":{
        ["key":"value",
        "key":"value",
        "key":"value"]
    }
}
```
So now that this works, I'm going to continue to build up my test page, which
is a really simple page just to send and receive websocket things

Wed 22 May 2019 11:45:22 ACST
-----------------------------
I really want to fix the errors which lead to the http server having its
connections borked and becoming unresponsive..

For that i need to know whether the main loop is still running. or if its just
skipping due to being stuck in a loop, or not thinking it has to perform
anything...

hmm, in order to get more information out of the wsRead function i need to
refactor it, i want to use the names i did in my discarded refactor

wsSend
wsRecv

so the first part is changing the signatures.

thats all done now, and i fixd some issues,
the http server problems wree because old connections would not be cleared
properly, there is still some work to do in this area but now whenever a
message is sent it does a check and removes old crufty connection entries.

now it doesnt halt anymore and i can fill up and ditch the connections  without
running into problems.. thre is still the possibility of a DOS because you can
simply hold as many connections as you like open, I think perhaps i will limit
it to one or two per device.. i'm not sure.. being able to perform both
websockt and POST at the same time would be OK. maybe one webxocket connection
per ip, and not limit POST messages.. still some work to do.

Mon 27 May 2019 14:33:28 ACST
-----------------------------
It appears that i am upto re-writing the sd card thing. when i look at the code
i fail to understand the motivation behind it, and its one of the oldest parts
of the current code base, so i was significantly less experienced when i wrote
it.

what do i want.
Accounting and general global control over all files at the same time.
a file class that uses the accounting that simplifies the access to the sdfat
library for  my purpose.

c++ community talks about zero cost abstractions a lot, so try to think about
that.

and considering how i feel about naming things, and the sd card is a card, i
want a name that is to do with dealing cards. like the man at the casino behind
the table. but to make the card dealer important he has to have a name.. and
there are enough superheroes that deal cards i just need to choose one, even
though i am not familiar with any of them.. or i could find any gambling greek
god.. or ancient god in general. I like naming things after gods.. maybe
because they represent the inspiration of a project.

actually i hae a better idea, libraries are a much better fit, so library card,
is a better analogy for th sd card thing. i could name it after a place, like
alexandria, or ..
fascinating stuff: https://en.wikipedia.org/wiki/Library_of_Alexandria

unfortunately that doesnt really help me come up with a name.

This might be more useful: https://en.wikipedia.org/wiki/Librarian
fuck their names are hard to write and read, so i'll just go with a generic
librarian for the filesystem, as the librarian keeps things in order. there
were not mentioned any prominent librarians, just the kings who created the
libraries.

everything is a file, and we have the librarian who collects and organises the
files.

which means i dont ever want to have a file be opened on its own.. only private
constructors.

Tue 28 May 2019 00:07:16 ACST
-----------------------------
I finally ran into memory limits, so there are a number of thoughts i've been
having in regards to ways to reduce overall memory consumption.

* split functions into smaller pieces, that way when the stack is built its
  made of smaller items.
* research how to make zero cost abstractions so that we can reduce the
  indirection overhead
* flatten the stack as much as possible.
* dont use new and delete so we can avoid dynamic memory overhead
* reduce string lengths
* reduce variables
* slim down structs and classes to bare minimum
* reduce scope of some of the things, like the logger.


I need to work on some of my documenation too.

Tue 28 May 2019 11:16:15 ACST
-----------------------------
SO i ran into some trouble last night with the SDLib class, where when it
deleted the FILE * pointers the system would reset. After some soul searching I
went back to basics, what was the purpose of the SDLib in the first place? so
that i can halt SD Logging whilst i turn the device off, or remove the SD card
so as to avoid corruption.

that appears to be a very specific case, and so for all other cases, if the
file handle is opened and closed within the same stack frame, then i will forgo
any accounting of the handles. I know the will clean up after themselves once
the function finishes.

I'll still use SDLib to get the sd subsystem process accounting, just not file
references

Tue 28 May 2019 22:17:28 ACST
-----------------------------
Initial tests are promising.

Whilst reading the logs trying to understand what is happening in what order, I
have this logging context changes happening all over the place, its just a mess
of annoyance, and clearly redundant bullshit. so i'm ripping it out in favour
of making it more laborious to write, but much easier to GROK.

I've also just had an idea how to get rid of most of the complexity of the
logging library by using nested macro's, which also gives the benefit of being
able t be compiled out. I really should have a look at constexpr for these
types of things. its exactly what its for.

Fri 31 May 2019 18:24:40 ACST
-----------------------------
I've thought more about this logging issue, and i think most of it can be
marked constexpr anyway. for instance the registration of the logging contexts
all gets done compile time, i can get rid of a bunch of macros for each context
using if constexpr when we eventually get to shift to c++17, but for now it can
be separate macros. lets start with the things that can be constexpr.

Sat 01 Jun 2019 00:31:43 ACST
-----------------------------
The above logging things are kind of a distraction, as the code works as is,
and i should be implemening the sensors into the framework and building up
thecommands.. adding a longer LED strip and mounting the device into the
ceiling. I should get this one in a working state, and buy another to prototype
with, if the changes are OK the push to the one that is alrready working when
done.

so what does that look like?
firstly its to connect the sensors to the correct pins and load up the
libraries to initialise them and see if that works OK.

Then get some very rudimentary realtime graphs up and running.

hmm which reminds me i still dont have historical data.

or automated pulling of the data  to the aggregate

or the aggregate.. gah.. so much to do.

Also solve the problem with PUT not adding line endings to the stream. why?...

OK so I thought of a way to prevent the depth of the call stack from
increasing, If i give each connection a state, which is defined by a function
pointer, when the function alters the state of the object it does to by the
function pointer, and when its done it changes the state to a new function
pointer.

ok so thats pretty rough, it might work depending on the complexity of the
state, making sure all the data members are in the right way before changing
it.. but it will certainly solve the stack tree depth increase. then it becomes
evaluate in round robin, where the unit of measurement is the time it takes to
complete a state change. a state can also change to itself.

I will think about doing that if i can later.. but what i really want to be
able to do is measure my memory usage. I wish i could analyse the binary with a
flame graph.

Maybe i can do that myself with some instrumentation which i can make optional
with a macro..i would do it with constexpr but hmm... its definitely a
possibiloty.. a singleton which increments and decrements on the entry to a
function call, and shows the memory output. wouldnt be that hard i dont think.
and would provide me with the necessary information to make an informed choice.


Sun 02 Jun 2019 18:34:27 ACST
-----------------------------
I'm thinking of including a query function into the periphery modules such that
they can expose functionality to an interface builder. so when the website
loads, it goes into query mode getting feedback from the beholder, and builds
the interface based on what information it gets.

for instance, the voltage sensor would provide
* realtime graph of voltage
* calibration input

that might look like
```json
[{
    "name":"battery"
    "elements": [
        {
            "id":"voltage",
            "type":"graph_rt"
            "domain":[0,15]
        },
        {
            "id":"calibrate",
            "type":"input"
        }]
},
{
    "name":"lights",
    "elements": [
        {
            "id":"off",
            "type":"button"
        },
        {
            "id":"on",
            "type":"button"
        }
        {
            "id":"colour",
            "type":"input"
        }]
}]
```

But that text is getting a bit long, so I might have to split it up into
messages per item, sort of like the interface can be built on the fly. if a
message is received it would build what it needs. something to think about for
the future at the moment. I want to get calibration of the voltage working.

Wed 05 Jun 2019 00:24:42 ACST
-----------------------------
One of the problems that is now being tackled is how to fragment the sending
and receiving of messages, for the purpose of not holding up the system.

there is an in built facility within websockets for fragmentation, which allows
control frames to be used, and i think that its worth implementing inmy context
since i have to do an implementation anyway. It might be slightly more bits to
send in total, but it will be closer to standard.

Something that immediately came to mind was how to load balance the four
connections i have, if each get a certain time slice of the bandwidth, then if
i had to split that slice to be either send or receive that wouldnt make sense
as a ping needs a pong very fast. I think within each client there needs to be
both a send and a receive state simultaneously.

So what are the current states that a websocket connction can be in? fuck i got
to sleep. only 11% battery.

Wed 05 Jun 2019 14:44:58 ACST
-----------------------------
states:
* HTTP_LISTEN
* HTTP_GET
* HTTP_GET_DIR
* HTTP_GET_FILE
* HTTP_GET_WS - upgrade request
* HTTP_HEAD,
* HTTP_POST
* HTTP_PUT
* HTTP_DELETE,
* HTTP_CONNECT,
* HTTP_OPTIONS,
* HTTP_TRACE,
* HTTP_PATCH,
* HTTP_CONTINUE - receives continue message
* WS_LISTEN
* WS_RECV
* WS_SEND
* WS_PING
* WS_PONG
* WS_CLOSE

Receiving large websocket frames will need to be pushd to SD memory rather than
dynamic memory. or be rejected.


Tue 25 Jun 2019 14:19:25 ACST
-----------------------------
Hmm this is interesting, whilst working on virtual paths for http GET requests
it makes me think i should split out all of the http GET requsts into their own
handlers.

So there is a default handler for SD paths, a websocket upgrade handler for
websocket paths, which would also be the same for virtual paths.
It would allow adding any type of handler for a path, just register it and it
will push off the http get request to that server module.

I guess this is why i have been procrastinating on this project for a few days,
because the task appears to be monumental and yet another re-write of the http
server code to enable this extra functionality.

Its a good thing i have version control for this.

if i'm going to make  such a large change, then i might have to think about it
a bit more.

server checks clients in a round robin for new traffic.
getting the type of connection and the resource in line1.

hmm, this thought process conflicts with the state machine that i currently
have in place to help split the process into piecemeal so connections dont
block each other.

can i make the clients have further programmed state? given when needed?
or does the server track the state of the clients, keeping a record of what its
upto for the next loop.

if the client maintains tracking its own state, which is bullshit because the
server already needs to know the state of the client in order to destroy it.

maybe i need to formalise that, and make the server the tracker and modifyer of
state, hmm... what if the client has a new state, like 'hand off' which it
hands the data to the handler. or it hands itself to the handler for
processing.. i think thats a bad idea. but lets think about it.

if the server processes the client and the state is 'handler' then it can check
the resource string and pass to its proper handler. which is the same as what i
was doing before... hmm what does that look like in the code?

I was pretty unhappy with the closure of the connections anyway, so perhaps
this can come under the state diagram. but how does it get the handler of state
if the server isnt the one who decides what should be the process..

maybe the client can report to the server what opcode the initial request is,
and then the server can decide how to handle it..

so client gets the first line and reports to server the opcode, or the
resource.

then the server decides what to do and hands that back to the client to action.
that would keep the state limited to the first line of the request, which is
nice and small.


Wed 26 Jun 2019 19:01:32 ACST
-----------------------------
Thinking about now the fact that i have two separate systems for getting
information from the host. the websocket version, and the httpGET version.

Firstly I need to clean up the http resource handler registration so that it is
coherant.

then I really need to think about if i can split out the sd card and websockets
out into their own resource handlers, if not then they can stay the same, but i
suspect that it can be modular like that.

so both websockets and the httpGET are ignorant of their use cases, for
instance the resource handler could be added that isa redirect of a URL to a
separate webserver. like perhaps the parent aggregate.. but anyway i digress.

if i'm getting information from httpGET, i dont really need to get it from
websockets if i can just go and get the data from the javascript.

the other aspect is that i can create a URL that might do something like

* POST http://host/sys/thing&set=value

which will change the value of the thing. but i think that would no longer be a
GET request, but a POST request.

Thu 27 Jun 2019 10:43:45 ACST
-----------------------------
Or i'm thinking that I can register a handle on the websocket connection so
that any external app can handle all continuous data from the connection,
rather than read it and then send it to the app. i think that would work better
as then it has a direct influence on the outcome and my liibrary becomes a sort
of helper, not the whole deal.

I wonder if i can make that work with the current structure. it might allow
streaming aswell.

which leads me into the logical conclusion that I can make it register a
generic traffic handle on any sort of upgrade, doesnt even need to be anything
to do with http, but since http is the base, i will stick with that.. for a
minute there i was thinking that i could abstract http from the client, but
then how would the negotiation of the type of connection have.. so http is the
base, and then upgrades to connections to other types would be posssible,
websockt for instance..

I'm trying to wrap my head around this new type of thinking, its proving
ifficult, i want to be able to register handles for how to react to certain
types of requests or connections, I want to make websockets a streaming
interface, and i now think they are separate tasks.

So lets go back to just thinking about the special handling of resource paths
for now, If i want to specialise websocket access i can do that later.

OK so since both the websocket and the http requests will return a json string,
then i guess the construction of the json string can be abstracted away from
the method that does the sending.

I dont really want a separate switch to determine what is sent, so perhaps i
can modify my websocket command syntax to match the http method syntax so that
we can unify them

which means that the command would be something along the lines of
GET http://<host>/sys/object
POST http://<host>/sys/object&action=payload
POST http://<host>/sys/object
    - (in body of request)action=payload
    - This could also be a list of things?
websocket packet
'''json
{
    "path":"/sys/object", // same as in GET and POST resource field
    "action":"set/get",
    "value":value
}
'''

So now I have to think about the system in terms of how its structured and how
the paths should be made

If an entry is a folder, then list the contents of the folder.

/sys
/sys/time   - current time of the device
/sys/p/     - location of the peripherals
/sys/l/     - locatiton of the logs

maybe i can move forward on this by re-doing the current items. hmm it seems
not. the way the priphery is setup right now appears to be super dumb, in order
for this to work i think i need to make the periphery a first class object.

Sun 30 Jun 2019 23:52:25 ACST
-----------------------------
Just thought that i should brainstorm up an outline to use as a TOC for
documentation.

# Hyperion Overview
## what = Basic Description
## Why = Purpose
## Where = Use Cases
## How = Dependencies
## When = TODO, goals
## Who = Attribution

# User Guides
## Simple Assembly and Installation
### Beholder Setup
### Aggregate Setup
## Creating Custom Modules

# API Reference
## Peripheral Virtual Class Interface
## URL & Message Structure
## d3 Graph abstractions for realtime and historical data

# Code Documentation
## Dependencies
### httpws
### Lumberjack
## Aggregate
### SystemD services/scripts
### Database
### Utilities
## Beholder
### Logging
### httpws
### Periphery
## Peripheral
### Virtual Class Interface

Hmm, thats at least something better than what i have already. I think its time
to try to get some sleep.

Mon 01 Jul 2019 17:08:32 ACST
-----------------------------
So today I tried my hand at separating out the httpws and the lumberjack fiels
into their own respective libraries, and i have it compiling in a branch ok, i
havent tested functionality yet.

for some reason teh compiled code is larger by about 6%, not sure why.. I had
to delete a bunch of formatting code in the logger, which i will have to re-add
using a lambda or something along those lines, or a logging formatter object.

now i want to clean up the SD card stuff since its sort of hanging about
unused. and whilst trying to just strip it out completely i thought that it
might be better to turn it into its own peripheral, would only do basic
reporting i think, i wouldnt use it for writing to the sd card, just to get
information out of it.

i'm also using extern for the SdFat thing int he http library, which i may want
to switch out to be explicit in the future, because requiring an SD card is not
even strictly necessary, the serving of data could be separated out liek the
current virtual file paths are.

its fucking cold where i am.. shit.. i think i will go find home.


Tue 02 Jul 2019 13:02:44 ACST
-----------------------------
I've removed almost all the sdlib code, the only remains are an enable
function, I;ve re-worked the httpws and lumberjack libraries, and i've got all
functionality back up and running AFAIK, there are probably failures in there
somewhere..

Also my code is pretty messy, I might want to continue the cleanup efforts.

Wed 03 Jul 2019 16:01:01 ACST
-----------------------------
So i'm finally back on track after the detour to split out the libraries into
their own projects, i have the virtual paths working nicely and everything is
OKish.

Now i want to clean up the stupid each function in the middle of the mainloop
that does the ealuation of whether to log output to the SD card and whether to
log it to the websocket for realtime data.

each peripheral makes the descision as to the frequency of each, and i think
thats a good idea. If i am to get rid of the loop, do i make two loops? one for
realtime and one for historical?

I think i'm tending towards two loops through th peripherals. 
the hard part of this is where do i perform the operations, there are three
layers
main loop->periphery->peripheral
and two types of writes, both sd card and websocket.

I have to either hand the sdcard and the server up the chain, or  have to hand
the data back.

forcing teh server and the sdfat into the peripherals doesnt seem like a good
idea, so i think i will do everything at the perihpery lvel, hand the server
and filename up into the periphery, and hand the data back
so mainloop->periphery<-peripheral.

only if we write do we want to perform the necessary tests for log rotation, so
i will keep that separate too..

Funny thing is that i dont even want to open the log file if there is nothing
to write, so I think the periphery should have some available functions to say
whether there is any data available.

Thu 04 Jul 2019 10:45:14 ACST
-----------------------------
I'm thinking of splitting the project across boundaries so that i have
hyperion-aggregate
hyperion-beholder

because the aggregate can be installed separately? hm maybe its not  great
idea, just some random thoughts.

I do want to treat the aggregate folder like a full project though because it
willbe installed on a host as a single unit.

which means separate documentation, separate README etc, in that regard it
might be better to split it out into its own repo and add it here as a sub
repo.

I'm not even sure how i would go about installing it, i guess i will make a
pkgbuild script for it and see how that works. do it for myself and then if
anyone else wants to chime in on what they want thent hat will work.

moved aggregate notes and diary entries here as they should be in a central
location

So I was thinking that I need a service that will watch the watchers.
* systemd unit files for automatic start and periodic firing.
* python scripts to run
    - collect from connected beholders
    - cleanup/process data to shrink its filesize.
    - discovery of beholders and their various modules.
* wesocket proxy
* website
    - nicer interface
    - three.js for 3d interface.

The python script will need to either do http-get to find the list of files,
or make a websocket connection and request. It depends on what is more
efficient.

Once it has the data it needs to add it to a database, or appropriately
organised files, again depending on how i wish to present the data. I'm
thinking that perhaps a database is best.

Considerations are how easy it is to get into a website after that.

I'm not sure exactly how easy it is to make requests to get the data from a
databse, whether its a POST request, or another GET request as part of the
resources etc.. I will have to look into it as it will be the primary goal of
the database to give back relevant informationto populate the graphs.

Services
--------
So today I'm thinking about how the services on the aggregate should be
defined.

For instance, should discovery periodically run, or only on demand?

what are the actual services that need to exist?
* [Watch]    - Frequent checking and pulling of log information from the beholders
* [Discover] - Find out what beholders exist on the network
* [Process]  - sanitise the incoming data, squash the older data, derive
               additional information from th data

[Watch]
-------
This service should happen frequently, like every minute or so.
It will connect to the beholders and request the list of files
It will download the newer logs and delete the older logs
It will do basic processing on the newer logs and put them into the log store

[Discover]
----------
This is a one shot command that arps the network to get the list of devices,
does a quick sanity check on any IP's it finds
adds to the internal list of thing to watch
Downloads any configuration data from the beholders to add to the aggregate
interfaces like what modules it runs, their properties etc.
should also display what devices no longer exist that did exist before.

[Process]
---------
Squishes the data by taking averages over larger and larger periods depending
on the age of the data.
Derives furthr information from the statistics like increase in averages, or
decrease, comparison data between average weeks or days etc.

Thoughts so far
---------------
So what i'm seeing from writing this down is that I need some configuration
data that needs to be persistent that the services can write to.
I need a database like structure to keep all the data in.

I'm checking the Linux Filesystem Heirarchy Standard now, but i suspect that it
will go in /srv/hyperion, under which the database and the discovery data will
reside.

I'm thinking that /etc/hyperion will just list the location of the various
parts of the system, like where to store the database, i'm not sure what else
there really is timing of the timed srvices is controlled by systemd which has
a facility for overrides anyway.

reading the standard, /srv appears to be the correct location for my stuff, and
there is no restriction on what i have to put in my subfolder.

So basic configuration information that doesnt change goes into /etc
all variable data like the database, website, beholder list, and downloaded
configuration of the beholders etc, goes into /var
And the scripts themselves? I think /usr/share/hyperion would be their
location,, unless i want to make the executable by default then they should go
into wherever python programs normally go.. reviewing my current system it
appears that all applications put their scripts into their respective
/usr/share/<app> folder.

I have re-organised into the rough directory structure
I think i have most of the things covered that are needed.

Checkup
-------
So i just had a thought about how the aggregate can have some resposibility
over the beholders in terms of timing. if the beholders internal time is bogus,
perhaps it can update it? which makes me think that there might be other
circumstances where the aggregate can perform configuration changes to
thebeholders. but I'm also skeptical about this. adding more and more
functionality to the behodlers isnt necessarily a good idea.

Installation
------------
Its important that the aggregate portion of the project is made in such a way
that it is installable, which means i will need a PKGBUILD script in here
somewhere. That way i can test deplying it to my local machine, to my rpi etc.
Considering that the thing is mostly some scripts and a website, I think that
it should be fairly straight forward to deploy onto a target host, mostly just
copying files to the correct location. which makes me think that perhaps the
whole folder structure doesnt need to be present in the source, but still it
might be useful to have there as it is kind of informative in and of itself.
Wed 19 Jun 2019 10:43:21 ACST
-----------------------------
Now that my mind has had a day to ferment on this subject I think i can write
down whats needed.

systemd unit files to kick off scripts
    - watcher
    - process

python scripts to do things
    - watch
    - process
    - discover

website is a little more complex, it needs to gather the information that is
needed from the discover, and then use that to generate the main site, i can
use python for most if not all ofthis.

since the site will be mostly data driven, the generation of the modules would
come in at least a few flavours

Controls
realtime graphs
historical graphs

and now that i'm thinking of this it makes me think that i need to re-do the
beholder project so that its easier to implement for anyone, just include some
files, do a beholder.begin, add some modules and upload.

thats the general gist, but the beholder isnt at that state yet, the aggregate
still needs to collct histocail information.

OK, so I think that the first thing i should implement is the watch.py script
since it will be doing the bulk of the work to start with. i can simulat the
systemd units with just some loops on the command line till i need to install
it as a service.

The watch script should look at the configuration to know what IP's to query.
using that it should send a GET or POST request to get the list of new log
files that exist.
then GET the log files
then DELETE the log files

the log files should be processed slightly to separate out the different
sensors into their own things. and i'm still not sure whether i want to use a
real database, or flat files. so i'm going to start with flat files, see what
my limits are on that.

Once that is completed, I should be able to manually create histocail graphs in
google sheets etc. so yeah watcher first.

Sat 22 Jun 2019 13:13:18 ACST
-----------------------------
I've got the basics of a watcher script done, I can send requests from the
script usin the resuests python module. so now I have to figure out the method
that I wish to use to receive the data.

scemantically an http GET request with parameters seems like the most logical
choice, but i could also use a POST request. POST would be somewhat easier, but
at the same time, doesnt seem scemantically clearer. since i'm not really
handing anything payload to the beholder. If i was to set a variable then a
http POST would make sense. so i  think abot POST as a synonym for SET, so it
becomes a sort of GET and SET style scemantics.

If i am to support parameters then i need to re-do my analysis of GET requests.

So how do i want to make that happen generically? doing a GET for a file
resource on the SD card is scemantically easy, not much to think about

doing a GET on an arbitrarily named resource or unnamed resource, 

http://10.0.0.177/&


Looking at some documentation, I think that since the information is static it
should just be the usual URL syntax, but the paths need to be registered
somehow. either pass off everything after a foldername into a specific path.

like 10.0.0.177/periphery/
returns a list of periphery names
/periphery/system/ returns all the system objects?

basically treat it like the /dev/directory of a linux filesystem, just over the
http thingo, so the question becomes, how do i map the names to actions, and
how to i register the directory tree into the http server so it knows what to
provide?


I'm thinking something like registerResource which you specify an override
path, anything under that gets passed off to the object you registered.

the registered object just needs to have a function to run that handles the
specific use case, and can be multiple inherited the override.

Thu 04 Jul 2019 12:25:59 ACST
-----------------------------
Working on the start of uwsgi to get data to the web client. so far archlinux
diverges from the official documentation in its quickstart area I have to do
this instead of the first example

uwsgi --http :9090 --plugin python --wsgi-file uwsgi_application.py

There area  number of new things to learn here, and i am going to do it in
reverse order to what may seem correct.

* basic uwsgi serving data
* nginx reverse proxy
    * might aswell sort out beholder websocket multiplexing too.
* containerization of both nginx and uwsgi

to start with i want a static website to make requests to the uwsgi
application.

I think i can use the fetch api
https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch

OK so that works, but i get the expected cross origin errors when attempting t
get from a separate port, so i will have to get the nginx reverse proxy working
for this to be useful.

ok i'm incredibly stupid, it took me ages to figure out that if i use
http-socket in uwsgi i need to use proxy_pass in nginx, or i have to use socket
in uwsgi and uwsgi_pass in nginx.. because it sets the protocol for
communcation. like way too looong... :|

Mon 08 Jul 2019 20:11:01 ACST
-----------------------------
Whilst thinking about things after completing the documentation it dawned upon
me to think about power consumption of the arduino, i wonder if there is any
power saving options that could be taken advantage of.

Aand it looks like someone has already done a lot of work for me :D

https://learn.sparkfun.com/tutorials/reducing-arduino-power-consumption/all

Thu 25 Jul 2019 10:01:46 ACST
-----------------------------
OK so i have most of the basics sorted, things are getting to a point that i
might be able o stop implementing new things and work on polishing and process.

I have both real time and historical graphs of data in test pages, I have an
arduino logging data and providing it through a websocket and web server, and a
the implementation of aggregate, which i should upload to the raspberry pi to
make it real and working constantly.

There are tons and tons of items to polish, and then making it super user
friendly will come into that category. but i'ma bit burt out, and i still need
to create a video.. i should work on the script.

script
------
the premise of the script is a 30 second elevator pitch as defined by jono
carol, I need to sell the idea.

here are some of the key statements i want to put into the script

* for us to make correct descisions we need information, and the more accurate the
  information the better.
* giving people power to optimise their lives through easy to build systems to 
  analyse the real world 
* [DONE] easy enough for an ametaur to install, open enough for an expert to really
  make it fly

---

Hi I'm Samuel, I want to show off what I've been working on

Project Hyperion is a system for monitoring, and controlling things from a web
browser interface.

Its target market is home handy people, makers, and hackers who want to
install and use home automation and internet of things(IoT) capabilities in their
environments.

It is designed to be easy enough for an ametaur to install, and open enough for an
expert to extend.

It is built using common components, languages and frameworks so there are no
weird surprises.

I want to invite you to have a look, leave feedback and if you are inclined,
contribute to the project

---

Thats fucking boring as shit!!

---

So i'm building this home automation and monitoring system for my #vanlife and
i want help, Its made from arduino's and raspberry pi's and basic components
from adafruit and freetronics.

It uses web interfaces for everything and is designed to be plug and play with
separation of concerns abstracting away all the electrical
componentry behind http web gateways

I want it to be a dream for web designers to really get their hands dirty
making pretty graphs and infographic style visualisations of their lives that
update in realtime.

I want to reduce the pain for hardware hackers to create interfaces and
realtime graphs for their data logging and home automation projects

I'm putting it into my van #Vanlife so i can monitor all my shit, cause graphs are
fascinating and fun.

I have most of the basic functionality working for a proof of concept, and now
I'm refactoring and polishing and adding nicer and nicer things.

The overall design means I have probably bitten off more than I can chew but
its important to be ambitious in life, but I could really use your help.

---

OK so thats two versions, one that is dry and boring and the other more
animated but filled with jargon. can i merge them at all?


OK #script acheived

Sun 28 Jul 2019 10:41:52 ACST
-----------------------------
I think the next stage is to package up the aggregate so that i can deploy it
to the raspberry pi, for the most part i have been using the laptop for all of
my developpment so its a bit messy.

lets try to list all of the things that need to be done.
* install dependencies
* place all the files in the appropriate folders
* enable systemd service units

i mean that seems fairly simple from my perspective, but as JP says the devil
is in the details.

##### Aggregate Dependencies

* python >= 3.7
* python requests
* uwsgi
* nginx
* systemd

OK so thats fine so far, we'll see what breaks on a fresh build, i think i
still have base images of arch floating around. indeed i do, so i can sort of
fake it with an x86 based machine, but i wouldnt mind actually having an arm VM
to test with with arch-arm.

##### Files
* /etc/nginx/nginx.conf - additions? or just help documentation on how to make it
                          work?
* /etc/uwsgi/hyperion.ini - add to the emperor config somehow? make it work
                            with base setup.
* /usr/lib/systemd/system/units - straight copy
* /usr/

hmm it seems that there is going to be post install setup required regardless
of what i wish, so perhaps i can put things in super generic places and then
have an install document which will guide the user through the remainder of the
setup. unfortunately that will make updating kind of annoying, unless i use
symlinks for stuff.. but then ... hmm needs some thought.

Mon 29 Jul 2019 21:15:02 ACST
-----------------------------
realised i wasnt reporting bugs i was documenting incomplete code and planning.
so now i have pieces that dont work as intended but are semi complete, all over
the place thats ok, start logging bugs for the things that arent working as
intended.

Bug: if a websocket connection is running then the watcher script kills the
arduino and a reset is required.

Thu 15 Aug 2019 16:43:06 ACST
-----------------------------
I have split out the aggregate service and web frontend into its own
repository, as the things grow apart it makes more sense to split them out, and
now i have a conundrum as to whether to make this repo the beholder, or make it
a sort of parent repo for documentation and website or something more generic.

splitting out the beholder is what i am leaning towards.

Mon 19 Aug 2019 14:18:46 ACST
-----------------------------
complete re-arrangement of the source tree has happened. it makes the project
much cleaner. and i think will make it simpler to reason about it, which i've
been having trouble with recently.

Mon 19 Aug 2019 17:08:26 ACST
-----------------------------
workin gon frivolous LED situations.
Ideally I would like  the way commands in the peripherals to have both their
code and their documentation spawn from the same set of code. its a bit like
defining a set of commands and responsive sequences.

something like

command name = 'somehting'
function handle [](){}

but i wouldnt mind even separating out the set/get/eval type nature of the
situation..
name, type, data

that way when feeding the json from theh http or websocket it can quickly run
the associated command without having to perform arduous if tree lookups.

Basically I want a map i think with non linear traversal, its a tree structure
for sure, and i think its non binary tree where the nodes are arranged in terms
of text ordering. except i think thats overkill.. 

A sorted list would work, test the first character and move down if invalid,
except to increases search time, test the middle item, and choose the segment
which is higher or lower of the item, then repeat.

its a binary space partition, then the only problem i guess is hmm, nothing?

so if i were to make this binary tree and give it an invalid string, it cant
quit early, but if it does fail then you can use the results for prediction.
because they are the closest neighbours, i think its quite doable given my
current mind.. so how would it look?

fixed size, so i could use template values to construct it.

three pointers to elements - begin, end, current.

hmm lets put it into the utils file for now and see what i can flesh out.
https://stackoverflow.com/questions/26351587/how-to-create-stdarray-with-initialization-list-without-providing-size-directl

thats needed to change the creation of such things to look like this

auto arr = my_make_array<int>(1,2,3,4,5);

or in my case
auto mystringmap = make_stringmap<type>( ... )

but i would at least want it to look like this:
auto mystringmap = make_stringmap<type>(
    { "string", type },
    ... )

but i'm not sure if that would auto deduce the elements properly.

also where and how am i putting this into the code? i mean initally it was in
the peripheral code, with function pointers being the things that are added to
the array..

can i even do that at compile time? create an array of string and function
pointers? lets find out whats possible piece by piece..

Wed 21 Aug 2019 17:01:55 ACST
-----------------------------
Interestingly i found a c++ talk by json turner which includes information
about the above

* https://www.youtube.com/watch?v=sSlmmZMFsXQ&t=3909s

And my stack overflow question received some love by both me and another person
which provided at least something of a solution.

So now i have a sort of solution to the problem, I need to make a uniform
method of interpreting the URL and have the json document reflect that

I'm working with
http://domain_or_ip/vpath/path/resource?data=value&data2=value

which i have been interpreting as
```json
{
    "path":"path/resource",
    "data":"value",
    "data2:"value"
}
```
stripping off the protocol, domain, and vpath from the url and reinterpreting
the additional data as their own elements.

The way the hand off from the client back to the server to interpret the
results at the moment is inconsistent and it makes me think that i want to move
the SD card handler back into the server, that way i could map paths to sd card
folders if desired, or other things.. but having the resource handlers in the
server appears to make more sense.

then instead of the client deciding some things, and the server deciding
others, it would be the server which decides.

but then i get stuck on client state again, where each client needs to keep
track of whats happening anyway, so perhaps moving the resource handlers into
the clients? which seems to be just duplication of effort..

I do want to make the SD card access entirely independent as a resource handler
module.. but how would this work? maybe I should just go back to what i was
thinking before.

Fri 23 Aug 2019 00:43:58 ACST
-----------------------------
Finally got around to fixing the log stripping situation by using a nested
namespace

This is will all logging functions stripped:

* Sketch uses 91980 bytes (36%) of program storage space.
* Global variables use 2900 bytes (35%) of dynamic memory

This is with logging functions intact:
* Sketch uses 113144 bytes (44%) of program storage space
* Global variables use 3012 bytes (36%) of dynamic memory

I saw no discernable difference in the code execution time between stripped and
when the log level was quiet.

Mon 07 Oct 2019 17:56:51 ACDT
-----------------------------
OK so i have a basic modular interface up and running and i want to push the
contents to the arduino to update the website data.. i can do this
manually..but it would be nice to have a script to do all the dirty work for
me.

xsomething like 
$ deploy.py 10.0.0.177
which then goes and scrubs the contents of the arduino http stuff and replaces
it with the updated version.

First i have to document how to perform it the manual way.

I think firstly delete is in order

This works:
curl -vX DELETE http://10.0.0.177/<filename>

I'm pretty sure i havent coded recursive delete yet.

well that wasnt too hard, but its pretty raw, i mean you can delete the
contents of the SD card really easy. ho well.

Tue 08 Oct 2019 12:29:23 ACDT
-----------------------------
So there are a couple of things that are happening whilst im am building this
interface, and they all relate to the fetch api. basically i want to fetch on
demand resources as needed, but not if they have already been fetched.

instead of writing this over and over in all the various init functions and
libraries, i want to create a function that will do it for us.

it will basically check the dom for pre-existing load, and abort if it already
finds the item, or it will load it up.. it should have an option to reload the
resource if incase it has changed.

it will hav to go into the TODO document because i'm still working on having
all of this information work.

because of this, there will be a sort of library which will have to be loaded
for any of these tools that utilise this functionality.. which means..

i have to create a hyperion-beholder.js utility library. i hope this doesnt
grow very large..

Fri 11 Oct 2019 13:06:24 ACDT
-----------------------------
Whilst thinking of a way to recursively load the dependencies of my javascript
files such that it performs a fetch on load, and doesnt re-fetch resources it
comes to mind that a top level fetch request needs to wait for all sub level
fetches to complete before it resolves itself.

fetch->fetch->fetch->fetch... if the chain is broken then the top level cannot
resolve and rejects.

thats for single resource dependencies, but what if i have multiple
dependencies

fetch->fetch
     ->fetch
     ->fetch->fetch

the same would apply, but if any of the three fails, then it must reject..

there is also the problem of having two such fetches perform their checks and
resolve at the same time, possibly creating multiple fetch requests at the same
time.. and causing some weird shit.. but in think in practice its unlikely..
hell lets just plow ahead anyway and see haha.

how can i nest promises?

OK first concept is promise chaining, where the result of the promise is
another promise. ive used this without thinking in the fetch command to resolve
a file which then provides a response which can be sent back with response.json() promise etc.

so how would that look for my dependency tree?

loadScript(url){
    how do i return the top level promise whilst chaining additional promises?
    
    fetch script.then
}
perhaps there are always two top level promises?

fetch, can chain new promises to script.
script

this site appears to be helping me understand the API
https://javascript.info/promise-chaining

that article is really good, but it doesnt show us how to perform the second
action, and calls them pyramids of doom, however thats exactly what i want to
build.

promise.all appears to be the thing i want.

ok so i have the building blocks, chaining with all.

now how do i put it into a thing.. and i'm sure someone has already built it,
or overbuilt it, with my restrictions i want to build it for myself.

load_script(url){
    return new Promise.all(
}

so how do i get it to happen automatically?

the idea is that each of my scripts contains a dependency list

graph = {
    deps = [dep,dep2,dep3...],
    name,
    description,
}

if this named structure exists then load up dependencies from it.. but then it
needs to be named something and not clobber the global namespace.. so how do i
go about that?
perhaps i could build up a global structure of files loaded as i load them..
like a tree that contains all the information for the scripts etc. does that
exist already?

i dont like the idea of arbitrary naming because of brittleness, i just want to
add a static class in that source file that can be referenced, but i dont think
thats posible.

so ihave to be careful with my names, and extract that from the source names,
and hope i dont clobber things.

source name = object name = object namespace

which means i need to rename all the source files to match their internal
function names
bme280.js becomes m_bme280 and i can have the things inside that strucutre.

ok my graph javascript needs to be renamed because its shit.
these auto dependencies are only for hard depenencies, when things are
definitely needed, not for when they are optional and loaded with a button.

Fri 11 Oct 2019 19:27:13 ACDT
-----------------------------
i dont even know where i am at with this, i have lost track of the current
position. which is like mytrain of thoguht dissapearing..

i am trying to build a promise and fetch based dynamic script loader.. where
each script loads up further scripts on demand.

the system relies on the fact that the modules will include a list of
dependencies to load up in a object with the same name as the filename.

module.js
module.deps = [deps,...]

when i perform a load module on a then it loads sub modules until there are no
promises left... i think i ned a prototype project to make it work.

Mon 14 Oct 2019 12:58:32 ACDT
-----------------------------
was using this site to help me understand promises, and such
http://taoofcode.net/promise-anti-patterns/

really got the final pieces of the puzzle as to how to chain promises together
for my script loading code.

Fri 18 Oct 2019 17:10:20 ACDT
-----------------------------
After fumbling around with my own module loader thing, i then discovered that
javascript has multiple definitions of pretty well defined modules of its own,
so i went down the rabbit hole for a while learning about how they all work.
and how they interact with d3.. it was frustrating to wade through the history,
dealing with AMD, commonjs, UMD, and then es6 modules. figure out how they
interacted with d3, and after implementing some tests i managed to settle on
es6 modules that required a slight modification to the d3 source files so that
they would work.

basically i would change the first occurrance of 'this' with 'self' which i
pulled off a stackoverflow post. it works and solves my problems.. i
modularised some of the components i was building and it has eventually greatly
simpligied things whilst teaching me all about promises and modules.

I'm just about to commit the files and i wanted to write this so that i can be
done with the investigation phase, and get back to implementing a modular
interface that is dynamically loaded.. whilst the dynamic loading in es6 hasnt
been standardised at this stage, i dont forsee much in the way of breaking
changes for future and browser compatibility appars ok. 

Fri 18 Oct 2019 22:54:57 ACDT
-----------------------------
OK so i've moved onto imlementing everything in test.html back into th emodular
design, and it really is making me think about how to load the data and what
html/module stuff should exist.

As modules on the beholder have shared state i can load that first in a sort of
common loader, then i can add on the module specific stuff afterwards.

Also since i am replacing index now anyway, i might aswell just do it and work
from that rather than this stupid secondary file.

Also I am thinking about how i want help documentation to appear, whether thats
in a separate document, or whether its in pieces which can be loaded up in
tooltips.. the bandwidth restrictions certainly have a huge influence on how i
wantt to build the site.. can i load it up as a secondary document and then
pull sections of it in as tooltips? perhaps with an enable tooltips checkbox.?

so much to think about. for now i want to split out the documentation into its
own file and implement connection to the websocket server, fetching of general
information

Fri 25 Oct 2019 16:15:15 ACDT
-----------------------------
I keep thinking about the blender VSE, i really hope that they get openscene
strip thing from pixar going because it decouples the framerate from the
footage, so worth the trouble.

anyway, i havent thought about any todo lists or troubles for a while as i have
been working on the interface for the beholder

and with that i now have a basic interface from the beholder itself. there are
of course a million things to change to make i nicer, including web friendly
ui. but for now it serves its purpose.

and its literally 288K which is good, but i recon i can get it smaller.
also the initial interface is much smaller. something lik 24K total.. which is
exactly what i was going for.. something small and fast.

the response time on the http requests and the websocket connection is tiny and
thats great.

there are big problems with logging at the moment, for one i think the log file
naming is dumb and not very helpful. it can log thousands of days and i have ti
clobbering logs at 10 days.

pretty sure there is no protection for the system log so it can grow till it
breaks.

the logging template things probably bloat my binary so i need to replace it
all with a pluggable log module.

sd card functionality needs a module of its own to show details

basically everything needs to be a module.

the aggregate needs to be re-done so that it actually fetches the logs..

the aggregate page needs to pull the modular interface files and create its own
interface from them providing a websockt proxy so that multiple beholders can
work together

the graphs need an overhaul to make them neater and not use up so much
resources when they arent being looked at.

i need to implement more sensors and controls including the automatic exhaust
fan control based on humidity

which means that i need to get aggregate analysis of real time data to trigger
events on other beholders and peripherals.

and those are the things just off the top of my head.

fuck what a large project i have chosen to do.

I need to naalyse mqtt and see if it fits in to the paradigm that i am using so
that i can interoperate with other systems, or us it to my advantage.

 I need to greate user friendly widgets that scale dynamically depending on the
viewing context ie laptop/mobile/tablet etc. and line up nicely.

i like the look of blenders interface so i would work towards creating the
esign like that.

i need to streamline the beholder interface, if i want to generate more things
i can do that in the future. i think removing the http section at the top is a
good start as i dont imagine it being a usable thing and yu can always craft
http instructions in the browser header.

but as i have just spent a large stint working on software and neglecting the
interior of the van i think i need to take a break and come back to this list
laater.. or perhaps right now transfer it to the TODO document..

OK done, I think I will come back to this list in a few weeks.

Tue 17 Mar 2020 20:45:04
-----------------------------
its been a while hey, little updates are happening as I wait for physical
things to be delivered in the mail.

Today I'm working on the web interface for the beholder, I sort of noticed that
the system peripheral doesn't have a very good command structure.

There are a bunch of sub modules which I was thinking of splitting into their
own peripherals but seeing as I haven't done that and I'm just getting familiar
with the code base again I thought I wouldn't worry about it too much.

Anyway, the commands currently are:
```cpp
{ "config",     &pSystem::cmd_config        },
{ "info",       &pSystem::cmd_info          },
{ "sd_enable",  &pSystem::cmd_sd_enable     },
{ "sd_info",    &pSystem::cmd_sd_info       },
{ "log_list",   &pSystem::cmd_log_list      },
{ "log_filter", &pSystem::cmd_log_filter    },
{ "net_info",   &pSystem::cmd_net_info      },
```
Which don't make a lot of sense to me in terms of their functionality.

I might need to re-work my configuration system in the near future and bring it
back from death.. But not today.

Each of these commands are invoked with a URL where it will be something like
`http://hostname/p/system/<command>[?key=value[&key=value[...]]]`

So each of the subsystems can have their own command, and then their key/value
pairs can be altered.

There is also the distinction between real-time data and other data so I'm
going to stick to the slow data for now, and I will make a separate list for
the real-time data.


```
- = read only
+ = read write
* info - basic system information
    - uptime
    - date time
    + name
    + custom info
* sdcard - SD card information
    - card size
    - utilisation
    - card type
    - CID Register
    - CST Register
    - OCR Register
    + enabled
    + format card
* logger
    - file list
    + level
    + output_mask
    + context_mask
    - output list
* network
    + status = up/down
    - hardware-type
    + subnet
    + gateway
    + dns
```

That same list again but with only real-time data
```
* info
    - period_ms
    - datetime
* sdcard
    - utilisation
    - card-busy
* logger
    - logger output
* network
    - connections
```

OK so lets get on this then. Starting with the info box.

Sun 22 Mar 2020 13:29:40
-----------------------------
So I've been working on the web interface, making widgets and things out of d3
to re-use in sections of the page, and whilst doing that i came across sass for
easier css writing. learning that it simply re-writes itself into conformtant
css and is used as a helper just shows me how shit css is and confirms my
hatred of website design.

For now i will not use it as the design is lean on purpose so should be easy to
manage, but I'll keep it in mind should i want to simplify my css later on when
it gets unwieldy

Sun 22 Mar 2020 15:07:54
-----------------------------
you know what, fuck vertical labels, css is shit. if i was to make it in svg it
would be  no brainer, have some actual fucking control of the document rathe
than this POS DOM manipulation that sucks balls and is unpredictable as all
fuck.
