/*
 * Ethermega beholder firmware
 */

// include system and library headers
#include <Ethernet.h>
#include <SdFat.h>
#include <RTClib.h>
#include <ArduinoJson.h>

// incude my librarires
#include <lumberjack.h>
#include <httpws.h>

// include project headers
#include "src/sdcontrol.h"
#include "src/util.h"
#include "src/config.h"
#include "src/periphery.h"
#include "src/peripheral.h"
#include "src/instrumentation.h"

/* Peripheral Modules */
#include "src/modules/p_system.h"
#include "src/modules/p_bme280.h"
#include "src/modules/p_voltmeter.h"
#include "src/modules/p_ledstrip.h"

using namespace LumberJack::LOGGERS;

// RTC
RTC_PCF8523 rtc;
DateTime time; //global time updated at the beginning of each loop

//SD Card accss
SdFat SD;

// HTTP/WebSocket server
httpws::Server server{80};

/*
 * Periphery Register
 FIXME this method of sizing the periphery before the items are added, and then
 adding them one by one feels fragile. perhaps use something similar to the way
 stringmap does it.
 */
Periphery periphery(4);
pSystem     p_system( "system" );
pVoltMeter  p_voltmeter( "battery", A0); //name, pin;
pLEDStrip   p_ledstrip( "leds", 6, 50 ); //name, pin, led count;
pBME280     p_bme280( "bme280" );


void setup() {
    //Logging facility
    auto &logger = LumberJack::Logger::get();
    logger.registerContext( F("INIT") );

    //Configuration
    auto &conf = Config::get();

    /* Initialise Serial port communication
     * ==================================== */
    Serial.begin( conf.sp_baud );

    //RTC
    rtc.begin();
    // following line sets the RTC to the date & time this sketch was compiled
    if( !rtc.initialized() ) rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));

    /* Initialise LumberJack logger */
    logger.setLevel( LogLevel::DEBUG ); //Always start in debug mode

    //TODO make the serial console a configuration setting via a pin jumper.

    /* Register logging on Serial port */
    logger.registerOutput( F("SERIAL"),
        []( const String &level,
            const String &context,
            const String &message ) -> bool {
                (void)context;
                Serial.print( level );
                Serial.print( " - " );
                //Serial.print( context );
                //Serial.print( " - " );
                Serial.println( message );
                return true;
        });

    /* SD Card
     * ================== */
    if( SDEnable() ){
        // delete old syslog
        if( SD.remove( "syslog" ) ){
            SD.vwd()->sync();
        } else {
            LOGE( F("unable to remove old syslog") );
        }
        /* register SD Card Logging */
        logger.registerOutput( F("SDCARD"),
            []( const String &level,
                const String &context,
                const String &message ) -> bool {
                    (void)context;
                    static File file( "syslog",
                            O_CREAT | O_WRONLY | O_TRUNC | O_APPEND | O_SYNC );

                    if(! file.isOpen() ){
                        LOGE( F("syslog is not open for reading") );
                        return false;
                    } else {
                        file.print( level );
                        file.print( " - " );
                        //file.print( context );
                        //file.print( " - " );
                        file.println( message.c_str() );
                    }
                    return true;
            });
    } else {
        LOGE( F("SD Card Error") );
        LOGE( F("Unable to create SD Library") );
        // FIXME use this value somewhere SD.cardErrorCode();
        // TODO Add SD Error Code and data to headers.
    }

    //FIXME Load configuration ASAP, perhaps create a dependency graph in a
    //draping app like draw.io so that you can make sense of when things can be
    //loaded
    if( SDIsEnabled() ) conf.load();
    LOGI( conf.toString() );


    //Ethernet
    Ethernet.begin( conf.mac, conf.ip, conf.dns, conf.gateway, conf.subnet );
    //FIXME replace this logging with the system json net_info function
    LOGI( F("MAC        "), mac2str( conf.mac ) );
    LOGI( F("IP Address "), Ethernet.localIP() );
    LOGI( F("Subnet Mask"), Ethernet.subnetMask() );
    LOGI( F("Gateway    "), Ethernet.gatewayIP() );
    LOGI( F("DNS Server "), Ethernet.dnsServerIP() );

    //TODO search for a configured aggregate to register with

    //WebSocket/HTTP
    server.registerResourceHandle( F("/p/"), &periphery );
    server.registerWebsocketHandle( &periphery );
    server.begin();

    logger.registerOutput( F("WEBSOCKET"),
        []( const String &level,
            const String &context,
            const String &message ) -> bool {
                (void)context;
                StaticJsonDocument<256> doc;
                DateTime now = rtc.now();
                doc[ F("path")     ] = F("log/syslog");
                doc[ F("datetime") ] = dateString( now ) + timeString( now );
                doc[ F("level")    ] = level;
                //data[ F("context") ] = context;
                doc[ F("message")  ] = message;
                server.wsBroadcast( doc );
                return true;
        });

    // Run initialisation for any of the periphery.
    periphery.add( &p_system );
    periphery.add( &p_voltmeter );
    periphery.add( &p_ledstrip );
    periphery.add( &p_bme280 );
    periphery.setup();

    LOGI( F("Initialisation Completed\n") );
    //TODO remove log context init.
    //TODO change logging mode to whats configured.
    logger.setContext(0x00);
}

//TODO split the loop() into smaller unique units to cut down on memory usage
void loop() {
    INSTRUMENTATION

    // evaluate any peripheral data requirements
    periphery.eval();

    //generate realtime report for websocket connections
    if( periphery.rtAvailable() ){
        periphery.report_rt( server );
    }

    //generate report for sd card
    if( periphery.sdAvailable() ){
        SdFile sensorlog( "sensorlog", O_CREAT | O_WRONLY | O_APPEND );
        if( !sensorlog.isOpen() ){
            LOGE( F("Unable to open sensorlog") );
            return;
        }

        ///send the file to the periphery for writing
        periphery.report_sd( sensorlog );

        //check whether we need to rotate
        static int log_increment = 0;
        if( sensorlog.fileSize() > 131072 ){ // 128Kb
            log_increment++;
            if( log_increment > 10) log_increment = 0;

            String newpath = String( F("sensorlog_") ) + log_increment;
            if( SD.exists( newpath.c_str() ) ){
                if( !SD.remove( newpath.c_str() ) ){
                    LOGE( F("Unable to delete"), newpath );
                }
            }

            if( !SD.rename( "sensorlog", newpath.c_str() ) ){
                LOGE( F("Unable to rename sensorlog to"), newpath );
            }
        }
        sensorlog.close();
    }

    // Check httpws status and respond to requests.
    server.statusReport();
    server.read();
}
