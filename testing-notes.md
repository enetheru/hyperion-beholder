test suite thoughts
===================
So i have gotten to the size that i cant keep track of all the things i ned to
check for regressions etc.

So lets kind of brainstorm about what needs to go into this folder.?

* HTTP requests
* websocket requests and upgrades
* c++ functions
* javascript objects

I'm still using the arduino toolchain and its really pathetic, I long for cmake
and CI style workflow, but i cant do that yet as i am focusing on the
functionality. but it would solve a lot of problems

Some of the other tests may require human oversight.

The current method i am using for testing is when i want to make some changes
to a component but i want to iterate over that before introducing it into the
main codebase, so the things in this directly already are simply test cases to
work out whether the idea works before migrating the code into the main
codebase and re-doing all dependencies.

i'll think about this again later


PUT
---
use curl to put a single file on the root of theSD
use curl to put a single file in a subfolder

GET
---
test both PUT files exist and their contents match the source

DELETE
------
delete the file on the root of the drive
delete the subfolder the nested file resides on

test that GET requests result in 404.

Loaf test page
--------------
Run through each test in turn and check their output.

tests
* sending each packet size
* 

