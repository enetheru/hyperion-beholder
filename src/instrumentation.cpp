#include <MemoryUsage.h>
#include <lumberjack.h>
#include "instrumentation.h"

using namespace LumberJack::LOGGERS;

namespace instrumentation {
    void Bugle::start(){
        stack_depth++;
    }

    void Bugle::end(){
        stack_depth--;
    }

    void Bugle::report(){
        LOGZ( F("stack_depth"), stack_depth );
        LOGZ( F("Free Ram: "), mu_freeRam() );
    }
};


