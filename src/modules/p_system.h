#ifndef P_SYSTEM_H
#define P_SYSTEM_H

#include <ArduinoJson.h>
#include <RTClib.h>
#include "../peripheral.h"
#include "../stringmap.h"

/* System peripheral
 */
class pSystem : public Peripheral
{
    DateTime uptime;
    unsigned int last_ms = 0;
public:
    pSystem( String name ) : Peripheral( name ) {}

    // Required Functions
    void setup();
    void eval() {}
    void report_rt( JsonDocument &doc );
    void report_sd( JsonDocument &doc );
    void request( JsonDocument &doc );

    /* commands */
    void cmd_config     ( JsonDocument &doc );
    void cmd_info       ( JsonDocument &doc );
    void cmd_sdcard     ( JsonDocument &doc );
    void cmd_logger     ( JsonDocument &doc );
    void cmd_network    ( JsonDocument &doc );

    using FuncPair = void (pSystem::*)(JsonDocument &);
    StringMap< FuncPair, 5 > funcs{{
        { "config",     &pSystem::cmd_config        },
        { "info",       &pSystem::cmd_info          },
        { "sdcard",     &pSystem::cmd_sdcard        },
        { "logger",     &pSystem::cmd_logger        },
        { "network",    &pSystem::cmd_network       },
    }};
};

#endif //P_SYSTEM_h
