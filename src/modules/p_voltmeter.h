#ifndef P_VOLTMETER_H
#define P_VOLTMETER_H

#include "../peripheral.h"
#include "../stringmap.h"

/* 
 * A simple 12v analog sensor
 *
 * I want to make this more generic so that you can plug in values forthe
 * voltage ratio from your volrage divider cirtuit, and the read value
 * difference, etc, make it generic and nice.. then made the class much more
 * aligned with the nature of c++, restrict what is unnecessary add all
 * remaining operators and functions, or delete them..
 */
class pVoltMeter : public Peripheral {
    int pin;
    float calibration = 1.0;
    float last_read;
public:
    pVoltMeter( String name, byte pin ) : Peripheral( name ), pin( pin ){}

    /* Required Functions */
    void eval();
    void setup() {}
    void request( JsonDocument &doc );
    void report_rt( JsonDocument &doc );
    void report_sd( JsonDocument &doc );

    /* commands */
    void cmd_calibrate( JsonDocument &doc );
    void cmd_volts( JsonDocument &doc );
    void cmd_raw( JsonDocument &doc );

    using FuncPair = void (pVoltMeter::*)(JsonDocument &);
    StringMap< FuncPair, 3 > funcs{{
        { "calibrate", &pVoltMeter::cmd_calibrate },
        { "volts",     &pVoltMeter::cmd_volts     },
        { "raw",       &pVoltMeter::cmd_raw       }
    }};
};

#endif //P_VOLTMETER_H
