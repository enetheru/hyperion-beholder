#include <lumberjack.h>
#include "p_bme280.h"
/*
 * BME280 Temperature, Himidity, Airpressure Sensor Functions
 */

using namespace LumberJack::LOGGERS;

void
pBME280:: setup() {
    if( !bme.begin() ){
        LOGE( F("Unable to setup BME280 sensor") );
        return;
    }
    enabled = true;
    rt_freq = 1000ul;
    sd_freq = 1000ul * 60ul * 5ul;
}

void
pBME280::report_rt( JsonDocument &doc ) {
    cmd_status( doc );
}

void
pBME280::report_sd( JsonDocument &doc ) {
    cmd_status( doc );
}

void
pBME280::request( JsonDocument &doc ) {
    {
        String output;
        serializeJsonPretty( doc, output );
        LOGO( name, F("received:"), output );
    }

    //strip the name to get the command string
    String cmd_str = doc[ F("path") ].as<String>().substring( name.length() + 1);
    LOGO( F("Command:"), cmd_str );

    // invoke command, else list options
    FuncPair cmd_func;
    if( funcs.find( cmd_str, cmd_func ) ){
        (this->*cmd_func)(doc);
    } else {
        //change this to something like funcs.list();
        doc[ F("path") ] = name;
        doc[ F("response") ] = String( F("Unknown Command: ") ) + cmd_str;
        JsonArray list = doc.createNestedArray( F("options") );
        for( auto i : funcs.list )
        list.add( i.first );
    }
}

void pBME280::cmd_config( JsonDocument &doc ){
    if( doc.containsKey( F("rt_freq") ) )
        rt_freq = doc[ F("rt_freq") ].as<int>();
    doc[ F("rt_freq") ] = rt_freq;

    if( doc.containsKey( F("sd_freq") ) )
        sd_freq = doc[ F("sd_freq") ].as<int>();
    doc[ F("sd_freq") ] = sd_freq;

    if( doc.containsKey( F("slp") ) )
        sealeavelpressure_hpa = doc[ F("slp") ].as<float>();
    doc[ F("slp") ] = sealeavelpressure_hpa;
}

void pBME280::cmd_status( JsonDocument &doc ){
    doc[ F("temp") ] = bme.readTemperature();
    doc[ F("pres") ] = bme.readPressure() / 100.0F;
    doc[ F("humi") ] = bme.readHumidity();
    doc[ F("alti") ] = bme.readAltitude( sealeavelpressure_hpa );
}
