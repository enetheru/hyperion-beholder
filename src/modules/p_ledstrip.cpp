#include <lumberjack.h>
#include "p_ledstrip.h"
#include "../stringmap.h"

using namespace LumberJack::LOGGERS;

// Hex string like #FFFFFF conversion to a uint32_t colour value
static uint32_t
rgb2col( String hex ){
    LOGO( "HEX", hex );
    if( hex[0] == '#' ) hex = hex.substring( 1, 7 );
    if( hex[1] == 'x' ) hex = hex.substring( 2, 8 );
    LOGO( "HEX", hex );

    String hex_col;
    //pull out the r value and convert it to an integer
    hex_col = hex.substring( 0, 2);
    uint8_t r = strtol( hex_col.begin(), nullptr , 16 );
    LOGO( "R", r );

    //pull out the g value and convert it to an integer
    hex_col = hex.substring( 2, 4);
    uint8_t g = strtol( hex_col.begin(), nullptr , 16 );
    LOGO( "G", r );

    //pull out the b value and convert it to an integer
    hex_col = hex.substring( 4, 6);
    uint8_t b = strtol( hex_col.begin(), nullptr , 16 );
    LOGO( "B", r );

    return Adafruit_NeoPixel::Color( r, g, b );
}

static uint32_t
rgbv2col( String hex ){
    //TODO
    return Adafruit_NeoPixel::Color( 255, 255, 255, 128 );
}

static uint32_t
hsv2col( String hex ){
    //TODO
    return Adafruit_NeoPixel::ColorHSV( 255, 255, 255 );
}

String
col2rgb( uint32_t col ){
    const char map[16] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
    String rgb{ "000000" };

    rgb[0] = map[ (col & 0x00F00000) >> 20 ];
    rgb[1] = map[ (col & 0x000F0000) >> 16 ];
    rgb[2] = map[ (col & 0x0000F000) >> 12 ];
    rgb[3] = map[ (col & 0x00000F00) >>  8 ];
    rgb[4] = map[ (col & 0x000000F0) >>  4 ];
    rgb[5] = map[ (col & 0x0000000F) >>  0 ];
    return rgb;
}

void
pLEDStrip::setup(){
        // When we setup the NeoPixel library, we tell it how many pixels, and
        // which pin to use to send signals.  Note that for older NeoPixel
        // strips you might need to change the third parameter--see the
        // strandtest example for more information on possible values.
        pixels.updateType( NEO_GRB + NEO_KHZ800 );
        pixels.updateLength( count );
        pixels.setPin( pin );
        pixels.begin(); // This initializes the NeoPixel library.
}

void
pLEDStrip::request( JsonDocument &doc ){
    {
        String output;
        serializeJsonPretty( doc, output );
        LOGO( name, F("received:"), output );
    }

    String cmd_str = doc[ F("path") ].as<String>().substring( name.length() + 1);
    LOGO( F("Command:"), cmd_str );
    FuncPair cmd_func;
    if( funcs.find( cmd_str, cmd_func ) ){
        (this->*cmd_func)(doc);
    } else {
        doc[ F("path") ] = name;
        doc[ F("response") ] = String( F("Unknown or missing command: ") ) + cmd_str;
        JsonArray list = doc.createNestedArray( F("options") );
        for( auto i : funcs.list )
        list.add( i.first );
    }

    LOGO( F("finished leds request") );
    return;
}


void pLEDStrip::report_rt( JsonDocument &doc ) { cmd_status( doc ); }
void pLEDStrip::report_sd( JsonDocument &doc ) { cmd_status( doc ); }

void
pLEDStrip::cmd_config( JsonDocument &doc ){
    doc[ F("pin") ] = pixels.getPin();
    doc[ F("num-pixels") ] = pixels.numPixels();
}

void
pLEDStrip::cmd_status( JsonDocument &doc ){
    //TODO get the array of pixels colours and send it
    doc[ F("brightness") ] = pixels.getBrightness();
    doc[ F("pixel-color") ] = col2rgb( pixels.getPixelColor( 0 ) );
}

void
pLEDStrip::cmd_set( JsonDocument &doc ){
    if( doc.containsKey( F("help") ) ) {
        doc[ F("help")] = "Available options are: first, last, count, rgb, rgbv, hsv, brightness";
        return;
    }
    /* the Adafruit_NeoPixel library allows us to set pixels in a number of
     * ways
        - Individual pixels by RGB or RGBV or HSV
        - Fill a set of pixels from n to n+c with a uint32 packed RGB
        - Change Brightness of whole array using a single value
     * There are also various modification functions which might be useful to
     * take advantage of like sine8, gamma8, gamma32; but as yet I'm not sure.
     */

    int first = 0;
    if( doc.containsKey( F("first") ) ) first = doc[ F("first")].as<int>();

    int last = pixels.numPixels() -1;
    if( doc.containsKey( F("last") ) ) last = doc[ F("last")].as<int>();

    int count = 16384;
    if( doc.containsKey( F("count") ) ) count = doc[ F("count")].as<int>();

    // Make sure start + count isnt over the size of the array
    count = pixels.numPixels() - first < count
        ? pixels.numPixels() - first : count;
    doc[ F("count") ] = count;
    doc[ F("last") ] = first + count;

    auto colour = Adafruit_NeoPixel::Color(255,255,255); //white
    if( [&doc, &colour](){
        if( doc.containsKey( F("rgb") ) ){
            colour = rgb2col( doc[ F("rgb") ].as<String>() );
            return true;
        }
        if( doc.containsKey( F("rgbv") ) ) {
            colour = rgbv2col( doc[ F("rgbv") ].as<String>() );
            return true;
        }
        if( doc.containsKey( F("hsv") ) ) {
            colour = hsv2col(  doc[ F("hsv") ].as<String>() );
            return true;
        }
        return false;
            }() )
    {
        LOGO("Found one of egb,rgbv,hsv");
        pixels.fill( colour, first, count );
    } else {
        LOGO("no colour tag found");
    }

    if( doc.containsKey( F("brightness") ) ) {
        pixels.setBrightness( doc[ F("brightness") ].as<int>() );
    } else {
        doc[ F("brightness") ] = pixels.getBrightness();
    }
    pixels.show();
}
