#include <Ethernet.h>
#include <RTClib.h>
#include <SdFat.h>
#include <SdCard/SdInfo.h>
#include <ArduinoJson.h>

#include <lumberjack.h>
#include <httpws.h>

#include "p_system.h"
#include "../util.h"
#include "../sdcontrol.h"

extern RTC_PCF8523 rtc;
extern DateTime time;
extern SdFat SD;
extern httpws::Server server;

/*
 * System peripheral
 */

using namespace LumberJack::LOGGERS;

void
pSystem:: setup() {
    uptime = rtc.now();
    rt = true;
    sd = false;
}

void
pSystem::report_rt( JsonDocument &doc ) {
/* TODO
    * network
        - connections
*/
    // Info
    int loop_ms = millis() - last_ms;
    last_ms = millis();
    doc[ F("period_ms") ] = loop_ms;
    doc[ F("time") ] = dateString( time ) + " " + timeString( time );
    //SD Card
    auto card = SD.card();
    doc[ F("is_busy") ] = card->isBusy();
    //doc[ F("cluster_count") ] = SD.clusterCount();
    //doc[ F("free_cluster_count") ] = SD.freeClusterCount();

    //Network
    JsonArray sockets = doc.createNestedArray( F("sockets") );
    for( int i = 0; i < 4; ++i ){
        auto client = server.getClient(i);

        String result = "null";
        if( client.connected() ) result = "Connected";
        if( client.available() ) result = "Available";
        if( client.wsConnected() ) result = "Websockets";

        sockets.add( result );
    }
}

void
pSystem::report_sd( JsonDocument &doc ) {
    report_rt( doc );
}

void
pSystem::request( JsonDocument &doc ) {
    {
        String output;
        serializeJsonPretty( doc, output );
        LOGO( name, F("received:"), output );
    }

    //strip the name to get the command string
    String cmd_str = doc[ F("path") ].as<String>().substring( name.length() + 1);
    LOGO( F("Command:"), cmd_str );

    // invoke command, else list options
    FuncPair cmd_func;
    if( funcs.find( cmd_str, cmd_func ) ){
        (this->*cmd_func)(doc);
    } else {
        //change this to something like funcs.list();
        doc[ F("path") ] = name;
        doc[ F("response") ] = String( F("Unknown Command: ") ) + cmd_str;
        JsonArray list = doc.createNestedArray( F("options") );
        for( auto i : funcs.list )
        list.add( i.first );
    }
}

void pSystem::cmd_config( JsonDocument &doc ){
    if( doc.containsKey( F("rt_freq") ) )
        rt_freq = doc[ F("rt_freq") ].as<int>();
    doc[ F("rt_freq") ] = rt_freq;

    if( doc.containsKey( F("sd_freq") ) )
        sd_freq = doc[ F("sd_freq") ].as<int>();
    doc[ F("sd_freq") ] = sd_freq;
}

void pSystem::cmd_info      ( JsonDocument &doc ) {
    doc[ F("name") ] = name;
    doc[ F("uptime") ] = dateString( uptime ) + " " + timeString( uptime );
    //TODO datetime
    //TODO extra info
}

void pSystem::cmd_sdcard  ( JsonDocument &doc ) {
    if( doc.containsKey( F("enabled") ) ){
        auto enabled = doc[ F("enabled") ].as<bool>();
        if( enabled ){
            if(! SD.vwd()->isOpen() ) SDEnable();
        } else {
            if( SD.vwd()->isOpen() ) SD.vwd()->close();
        }
    }
    if( SD.vwd()->isOpen() )
        doc[ F("enabled") ] = true;
    else {
        doc[ F("enabled") ] = false;
        return;
    }

    auto card = SD.card();

    doc[ F("card_type") ] = card->type();
    doc[ F("card_size") ] = card->cardSize();
    doc[ F("volume_block_count") ] = SD.volumeBlockCount();

    doc[ F("cluster_count") ] = SD.clusterCount();
    doc[ F("blocks_per_cluster") ] = SD.blocksPerCluster();
    doc[ F("free_cluster_count") ] = SD.freeClusterCount();

    doc[ F("fat_type") ] = SD.fatType();
    doc[ F("blocks_per_fat") ] = SD.blocksPerFat();
    doc[ F("fat_count") ] = SD.fatCount();


    {
        cid_t cid;
        if( card->readCID( &cid ) ){
            /** Manufacturer ID */
            //unsigned char mid;
            //doc[F("card_mid")] = cid.mid;
            /** OEM/Application ID */
            //char oid[2];
            /** Product name */
            //char pnm[5];
            // doc[F("card_pnm")] = String( cid.pnm ); //FIXME
            /** Product revision least significant digit */
            //unsigned char prv_m : 4;
            /** Product revision most significant digit */
            //unsigned char prv_n : 4;
            /** Product serial number */
            //uint32_t psn;
            /** Manufacturing date year high digit */
            //unsigned char mdt_year_high : 4;
            /** Manufacturing date month */
            //unsigned char mdt_month : 4;
            /** Manufacturing date year low digit */
            //unsigned char mdt_year_low : 4;
            /** CRC7 checksum */
            //unsigned char crc : 7;
        }
        else {
            doc[ F("error") ] = "failed to read CID data";
        }
    }
    /* TODO
bool 	readCSD (csd_t *csd)
// operation condition register
// https://luckyresistor.me/cat-protector/software/sdcard-2/
bool 	readOCR (uint32_t *ocr)
 
bool 	readStatus (uint8_t *status)
*/
}

void pSystem::cmd_logger  ( JsonDocument &doc ) {
    LumberJack::Logger &logger = LumberJack::Logger::get();

    if( doc.containsKey( F("log_level") ) ){
        logger.setLevel( logger.str2Level( doc[ F("log_level") ] ) );
    }

    if( doc.containsKey( F("log_output_mask") ) ){
        //FIXME logger.setOutputMask( doc[ F("log_output_mask") ] );
    }

    if( doc.containsKey( F("log_context_mask") ) ){
        //FIXME logger.setContextMask( doc[ F("log_context_mask") ] );
    }

    doc[ F("log_level")         ] = logger.level2Str( logger.getLevel() );

    JsonArray log_contexts = doc.createNestedArray( F("log_contexts") );
    doc[ F("log_context_mask") ] = logger.getContextMask();

    JsonArray log_outputs = doc.createNestedArray( F("log_outputs") );
    doc[ F("log_output_mask")  ] = logger.getOutputMask();

    if(! SD.vwd()->isOpen() ){
        doc[ F("error") ] = F("SD Card Not Available");
        return;
    }

    JsonArray log_files = doc.createNestedArray( F("log_files") );
    {
        SdFile root( "/" ,O_READ);
        SdFile sub_file;
        char name[256];
        while( sub_file.openNext( &root, O_READ ) ){
            sub_file.getName( name, 256 );
            if( String(name).startsWith( F("sensorlog_") ) ){
                log_files.add( name );
            }
            sub_file.close();
        }
    }
}

void pSystem::cmd_network  ( JsonDocument &doc ) {

    //Make modifications with incoming data.
    if( doc.containsKey( F("mac_address") ) ){
        LOGW("changing mac address not implemented");
        //TODO Figure out how to make this work
    }
    if( doc.containsKey( F("link_status") ) ){
        LOGW("changing link satus not implemented");
        //TODO Figure out how to turn on and off the devic from here
    }
    if( doc.containsKey( F("local_ip") ) ){
        LOGW("changing local ip  not implemented");
        //TODO figure out how to change the ip address from here
    }
    if( doc.containsKey( F("subnet_mask") ) ){
        LOGW("changing subnet mask not implemented");
        //TODO figure out how to change the ip address from here
    }
    if( doc.containsKey( F("gateway_ip") ) ){
        LOGW("changing gateway ip not implemented");
        //TODO figure out how to change the ip address from here
    }
    if( doc.containsKey( F("dns_server_ip") ) ){
        LOGW("changing dns server ip not implemented");
        //TODO figure out how to change the ip address from here
    }

    switch( Ethernet.linkStatus() ){
        case LinkOFF:
            doc[ F("link_status") ] = F("down");
            break;
        case LinkON:
            doc[ F("link_status") ] = F("up");
            break;
        case Unknown:
        default:
            doc[ F("link_status") ] = F("unknown");
            break;
    }
    switch( Ethernet.hardwareStatus() ){
        case EthernetW5100:
            doc[ F("hardware_status") ] = F("WizNet5100");
            break;
        case EthernetW5200:
            doc[ F("hardware_status") ] = F("WizNet5200");
            break;
        case EthernetW5500:
            doc[ F("hardware_status") ] = F("WizNet5500");
            break;
        case EthernetNoHardware:
        default:
            doc[ F("hardware_status") ] = F("unknown");
            break;
    }

    //MACAddress(uint8_t *mac_address);
    doc[ F("local_mac") ] = F("FIXME");

    auto ip = Ethernet.localIP();
    String ipstr = String( ip[0] ) + "." + ip[1] + "." + ip[2] + "." + ip[3];
    doc[ F("local_ip")      ] = ipstr;

    ip = Ethernet.subnetMask();
    ipstr = String( ip[0] ) + "." + ip[1] + "." + ip[2] + "." + ip[3];
    doc[ F("subnet_mask")   ] = ipstr;

    ip = Ethernet.gatewayIP();
    ipstr = String( ip[0] ) + "." + ip[1] + "." + ip[2] + "." + ip[3];
    doc[ F("gateway_ip")    ] = ipstr;

    ip = Ethernet.dnsServerIP();
    ipstr = String( ip[0] ) + "." + ip[1] + "." + ip[2] + "." + ip[3];
    doc[ F("dns_server_ip") ] = ipstr;
}
