#ifndef P_LEDSTRIP_H
#define P_LEDSTRIP_H

//Headers for the lighting portion
// NEOPIXEL lights, this is just a temporary hack to get it out of the main
// source.

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

#include "../peripheral.h"
#include "../stringmap.h"

/*
 * A Simple command only interface to my strip LED lights
 * TODO inherit from adafruit neopixel lib to reduce overall & data code bloat
 */
class pLEDStrip : public Peripheral {
    int pin = 6;
    int count = 50;
    Adafruit_NeoPixel pixels;

public:
    pLEDStrip( String name, int pin, int count )
        : Peripheral( name ), pin( pin ), count( count ) {}

    //Required functions
    void setup();
    void request( JsonDocument &doc );
    void eval() {}
    // no reporting to be done as nothing changes rt and sd to true;
    void report_rt( JsonDocument &doc );
    void report_sd( JsonDocument &doc );

    // commands
    void cmd_config( JsonDocument &doc );
    void cmd_status( JsonDocument &doc );
    void cmd_set(    JsonDocument &doc );

    using FuncPair = void (pLEDStrip::*)(JsonDocument &);
    //c++17 offers auto template paremeter deduction
    StringMap< void (pLEDStrip::*)(JsonDocument &), 3> funcs{{
        { "config", &pLEDStrip::cmd_config },
        { "status", &pLEDStrip::cmd_status },
        { "set",    &pLEDStrip::cmd_set },
    }};

};


#endif //P_LEDSTRIP_H
