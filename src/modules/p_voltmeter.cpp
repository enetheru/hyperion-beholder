#include <lumberjack.h>
#include "p_voltmeter.h"

using namespace LumberJack::LOGGERS;

/* 12V Sensor Functions
 * */
void pVoltMeter::eval(){
    Peripheral::eval();
    last_read = map( analogRead(pin), 0, 1023, 0, 1499 ) * calibration / 100;
}

void pVoltMeter::report_rt( JsonDocument &doc ) {
    doc[ F("volts") ] = last_read;
}

void pVoltMeter::report_sd( JsonDocument &doc ) {
    doc[ F("volts") ] = last_read;
}

void pVoltMeter::request( JsonDocument &doc ){
    {
        String output;
        serializeJsonPretty( doc, output );
        LOGO( name, F("received:"), output );
    }

    //strip the name to get the command string
    String cmd_str = doc[ F("path") ].as<String>().substring( name.length() + 1);
    LOGO( F("Command:"), cmd_str );

    // invoke command, else list options
    FuncPair cmd_func;
    if( funcs.find( cmd_str, cmd_func ) ){
        (this->*cmd_func)(doc);
    } else {
        //change this to something like funcs.list();
        doc[ F("path") ] = name;
        doc[ F("response") ] = String( F("Unknown Command: ") ) + cmd_str;
        JsonArray list = doc.createNestedArray( F("options") );
        for( auto i : funcs.list )
        list.add( i.first );
    }
}

/* Commands
 * ======== */
void
pVoltMeter::cmd_calibrate( JsonDocument &doc ){
    if( doc.containsKey( F("value") ) ){
        calibration = doc[ F("value") ].as<float>();
    }
    doc[ F("value") ] = calibration;
}

void
pVoltMeter::cmd_volts( JsonDocument &doc ){
    doc[ F("value") ] = last_read;
}

void
pVoltMeter::cmd_raw( JsonDocument &doc ){
    doc[ F("value") ] = analogRead( pin );
}
