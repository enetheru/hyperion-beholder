#ifndef P_BME280_H
#define P_BME280_H
//Headers for the BME280
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

#include "../peripheral.h"
#include "../stringmap.h"

/*
 * Temperature Humidity Air-Pressure Sensor BME280
 */
class pBME280 : public Peripheral
{
    bool enabled = false;
    float sealeavelpressure_hpa = 0;
    Adafruit_BME280 bme;
public:
    pBME280( String name ) : Peripheral( name ) {}

    // Required Functions
    void setup();
    void report_rt( JsonDocument &doc );
    void report_sd( JsonDocument &doc );
    void request( JsonDocument &doc );

    /* commands */
    void cmd_config( JsonDocument &doc );
    void cmd_status( JsonDocument &doc );

    using FuncPair = void (pBME280::*)(JsonDocument &);
    StringMap< FuncPair, 2 > funcs{{
        { "config", &pBME280::cmd_config           },
        { "status", &pBME280::cmd_status           }
    }};
};

#endif //P_BME280_h
