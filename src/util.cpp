#include "util.h"

String timeString( const DateTime &dateTime ){
    String output;
    output += dateTime.hour() < 10 ? "0" : "" ;
    output += String(dateTime.hour());
    output += ":";
    output += dateTime.minute() < 10 ? "0" : "" ;
    output += String( dateTime.minute() );
    output += ":";
    output += dateTime.second() < 10 ? "0" : "" ;
    output += String( dateTime.second() );
    return output;
}

String dateString( const DateTime &dateTime ){
    String output;
    output += String(dateTime.year());
    output += "-";
    output += dateTime.month() < 10 ? "0" : "" ;
    output += String( dateTime.month() );
    output += "-";
    output += dateTime.day() < 10 ? "0" : "" ;
    output += String( dateTime.day() );
    return output;
}

String spanString( const TimeSpan &span ){
    String output;
    output += String( span.days() );
    output += "d ";
    output += String( span.hours() % 24 );
    output += "h ";
    output += (span.minutes() % 60) < 10 ? "0" : "" ;
    output += String( span.minutes() );
    output += "m ";
    output += (span.seconds() % 60) < 10 ? "0" : "" ;
    output += String( span.seconds() );
    output += "s";
    return output;
}

String mac2str( uint8_t * mac ){
    String ret = String( mac[0], HEX );
    for( int i = 1; i < 6; ++i ){
        ret += ":" + String( mac[i], HEX );
    }
    return ret;
}

String
bin2str( const uint8_t &bits ){
    String ret;
    for( int i = 0; i < 8; ++i ){
        if( bits & (1 << (7-i)) ) ret += "1";
        else ret += "0";
    }
    return ret;
}

// converts a mask to an integer or -1 for failure.
int bitPos( const uint8_t &mask ){
    uint8_t i = 0;
    for( uint8_t j = 0b00000001; j < 128; j <<= 1 ){
        if( j & mask ) return i;
        ++i;
    }
    return -1;
}

// returns the position of the first available bit or -1
int firstEmptyBitNum( const uint8_t &mask ){
    for( int i = 0; i < 8; ++i ){
        int test = 1 << i;
        if( test & ~mask) return i;
    }
    return -1;
}

// returns the bitmask of the first available bit from a mask or 0
uint8_t firstEmptyBit( const uint8_t &mask ){
    for( int bit = 1; bit != 256; bit = bit << 1 ){
        if( bit & ~mask )return bit;
    }
    return 0;
}

