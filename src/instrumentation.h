#ifndef INSTRUMENTATION_H
#define INSTRUMENTATION_H

#include <lumberjack.h>

/* this header is for adding or removing instrumentation so i can inspect how
 * deep the call stack goes, and how much memory is stil available at any one
 * time.
 *
 * what are the things i want to know?
 *  - stack depth
 *  - current memory
 *  - function name
 *  - time  might be a bit harder to pin down
 *
 * I dont want it for everything, just my function so whilst there are some
 * global things i can do with instrumentation using gcc instrumentation i can
 * also do it with manual macros for now.
 *
 */
#ifndef INSTRUMENTATION_STRIP
#define INSTRUMENTATION \
instrumentation::Ocarina ocarina(__FUNCTION__);
#else
#define INSTRUMENTATION
#endif

namespace instrumentation {
    using namespace LumberJack::LOGGERS;
    /*
     * Bugle singleton
     */
    class Bugle {
        /* Singleton boilerplate */
    private:
        Bugle() {}
        ~Bugle() {}

    public:
        static Bugle& get(){
            static Bugle instance;
            return instance;
        }
        Bugle( Bugle const& )         = delete;
        void operator=( Bugle const& ) = delete;

        /* normal functions */

        int stack_depth = 0;
        void start();
        void end();
        void report();
    };

    /*
     * Ocarina RAII thing.
     */
    struct Ocarina{
        Ocarina( const char * function ){
            LOGZ( F("===="), function , F("====") ); \
            Bugle::get().start();
            Bugle::get().report();
        }
        ~Ocarina(){
            Bugle::get().end();
            LOGZ( F("- - - - - - - -") );
        }
    };
};


#endif //INSTRUMENTATION_H
