#ifndef SDCONTROL_H
#define SDCONTROL_H

#include <lumberjack.h>

#define MAX_LOG_SIZE 134217700

bool SDEnable( int chip_select = 4 );
bool SDIsEnabled();
bool SDDisable();

#endif //SDCONTROL_H
