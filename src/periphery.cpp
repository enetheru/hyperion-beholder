
#include "periphery.h"
#include "util.h"

extern RTC_PCF8523 rtc;
extern httpws::Server server;

using namespace LumberJack::LOGGERS;


void
Periphery::splitURL( const String &url, JsonDocument &doc ){
    // path/[resource][?data1=value[&dataN=valueN]]
    int pos = url.indexOf( '?' );
    if( pos == -1 ){
        doc[ F("path") ] = url;
        return;
    } 

    doc[ F("path") ] = url.substring( 0, pos );
    String vars = url.substring( pos +1 );

    while( true ){
        pos = vars.lastIndexOf('&');
        pos = pos == -1 ? 0 : pos;

        String pair = vars.substring( pos == 0 ? 0 : pos +1 );
        int eq = pair.indexOf('=');
        if( eq == -1 ){
            doc[pair] = true;
        } else {
            doc[ pair.substring( 0, eq ) ] = pair.substring( eq+1 );
        }

        if(! pos ) break;
        vars = vars.substring( 0, pos );
    }
}

bool
Periphery::add( Peripheral *p ){
    if( pos >= size ){
        return false;
    }
    peripherals[pos] = p;
    pos++;
    return true;
}

void
Periphery::setup(){
    for( int i = 0; i < size; ++i ){
        peripherals[i]->setup();
    }
}

void
Periphery::eval(){
    time = rtc.now();
    rt = false;
    sd = false;
    for( int i = 0; i < size; ++i ){
        auto p = peripherals[i];
        p->eval();
        rt |= p->rt;
        sd |= p->sd;
    }
}

void
Periphery::report_rt( httpws::Server &server ){
    for( int i = 0; i < size; ++i ){
        auto p = peripherals[i];
        if( !p->rt )continue; //skip if it doesnt want to write
        StaticJsonDocument<256> doc;
        doc[ F("datetime") ] = dateString( time ) + " " + timeString( time );
        doc[ F("path")     ] = p->name;
        p->report_rt( doc );
        server.wsBroadcast( doc );
    }
}

void
Periphery::report_sd( SdFile &file ){
    for( int i = 0; i < size; ++i ){
        auto p = peripherals[i];
        if( !p->sd )continue; //skip if it doesnt want to write
        StaticJsonDocument<256> doc;
        doc[ F("datetime") ] = dateString( time ) + " " + timeString( time );
        doc[ F("path")     ] = p->name;
        p->report_sd( doc );
        serializeJson( doc, file );
        file.println("");
    }
}

void
Periphery::jsonHandle( JsonDocument &doc ){
    /* The path is split such that the first level is the peripheral in
     * question <peripheral>/ the remainder is upto the peripheral to
     * decide.
     */
    String path = doc[ F("path") ];       //resource

    //test for sub paths, and get name accordingly
    String p_name;
    int split_idx = path.indexOf( '/' );
    if( split_idx == -1 ){
        p_name = path;
    } else {
        p_name = path.substring(0, split_idx );
    }

    // find the peripheral by that name and send the request to it.
    for( int i = 0; i < size; ++i ){
        auto p = peripherals[i];
        if( p->name.equals( p_name ) ){
            p->request( doc );
            return;
        }
    }

    // else we provide a list of peripherals.
    doc[ F("response") ] = F("Unknown option");
    JsonArray list = doc.createNestedArray( F("options") );
    for( int i = 0; i < size; ++i ){
        auto p = peripherals[i];
        list.add( p->name );
    }
}

httpws::OpCode
Periphery::wsHandle( httpws::Client &client ){
    // TODO get rid of this data allocation by streaming directly from the
    // websocket client into a JsonDocument
    unsigned int size = client.wsLength();
    char data[ size+1 ];

    LOGO( F("Periphery Websocket handle") );
    LOGO( F("Message Length:"), size );
    LOGO( F("Position:"), client.wsPos() );

    client.wsRead( (uint8_t*)data, size );
    data[size] = '\0';
    LOGD( F("Message:"), data );

    //Process the incoming date into a json format
    StaticJsonDocument<256> doc;
    // Test if parsing succeeds.
    auto error = deserializeJson(doc, data);
    if( error ){
        LOGE( F("deserializeJson() failed: "), error.c_str() );
        return httpws::ERROR;
    }

    //jsonHandle also modifies the document for a return result.
    LOGO( F("jsonHandle") );
    jsonHandle( doc );
    //which we then send onto the clients
    LOGO( F("wsSend") );
    server.wsBroadcast( doc );
    LOGO( F("Finished wsHandle") );
    return httpws::NONE;
}

httpws::OpCode 
Periphery::httpHandle( httpws::Client &client ){
    //At this moment the only part of the request that has been received is
    //the first line indicating that it is a GET request, and that the
    //resource begins with the path we defined.

    //Finish reading the headers
    String header, value;
    while( client.nextHeader( header, value ) ){
        LOGI( header, F(":"), value );
    }

    StaticJsonDocument<256> doc;

    // process the url and put it in the json document
    splitURL( client.getResource(), doc );

    //handle it
    jsonHandle( doc );

    //send out the response
    String content;
    serializeJsonPretty( doc, content );
    client.httpOK( 200, "",
           String( F("Content-Length: ") ) + String( content.length() ),
           F("Content-Type: application/json") );
    client.println( content );
    client.print( F("\r\n") );
    return httpws::NONE;
}
