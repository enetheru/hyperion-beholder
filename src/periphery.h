#ifndef PERIPHERY_H
#define PERIPHERY_H

#include <Arduino.h>
#include <ArduinoJson.h>
#include <RTClib.h>
#include <httpws.h>

#include "peripheral.h"

class Periphery : public httpws::ResourceHandle, public httpws::WebsocketHandle {
    Peripheral **peripherals;
    int size = 0;
    int pos = 0;
    bool rt,sd;
    DateTime time;

    //internal utility functions
    void splitURL( const String &url, JsonDocument &doc );
    void jsonHandle( JsonDocument &doc );

public:
    Periphery( int size ) : size( size ){
        peripherals = new Peripheral*[size];
    }
    ~Periphery() {
        delete [] peripherals;
    }
    bool add( Peripheral *p );

    void setup();
    void eval();
    void report_rt( httpws::Server &server );
    void report_sd( SdFile &file );

    bool rtAvailable() const { return rt; }
    bool sdAvailable() const { return sd; }


    httpws::OpCode wsHandle( httpws::Client &client );
    httpws::OpCode httpHandle( httpws::Client &client );
};

#endif //PERIPHERY_H
