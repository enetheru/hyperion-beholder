#include "eth_util.h"

String mac2string( const uint8_t *in ){
    return      String( in[0], HEX )
        + ":" + String( in[1], HEX )
        + ":" + String( in[2], HEX )
        + ":" + String( in[3], HEX )
        + ":" + String( in[4], HEX )
        + ":" + String( in[5], HEX );
}
