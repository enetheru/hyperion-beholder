#include "config.h"

#include <ArduinoJson.h>
#include <SdFat.h>

#include <lumberjack.h>
#include "eth_util.h"
#include "instrumentation.h"

using namespace LumberJack::LOGGERS;
bool
Config::load(){
    INSTRUMENTATION
    File file( "conf.json", O_RDONLY );
    //FIXME handle errors
    StaticJsonDocument<512> doc;

    // Deserialize the JSON document
    auto error = deserializeJson( doc, file );
    if( error ){
      LOGE( F("Failed to read /conf.json, using defaults"));
    } else {
        // Get the root object in the document
        JsonObject root = doc.as<JsonObject>();

        // Copy values from the JsonObject to the Config
        // TODO copy the remaining names
        //if( root[ "name"    ] )name         = root[ "name" ].as<const char *>();
        //if( root[ "freq_rt" ] )freq_rt      = root[ "freq_rt" ];
        //if( root[ "freq_net" ] )freq_net    = root[ "freq_net" ];
        //if( root[ "freq_sd" ] )freq_sd      = root[ "freq_sd" ];
        //if( root[ "log_level" ] )log_level  = root[ "log_level" ];
        //LJ::Logger::get().setLevel( log_level );
    }
    return true;
}

bool
Config::save(){
    INSTRUMENTATION
    File file( "conf.json", O_CREAT | O_TRUNC | O_WRONLY );
    file.write( toString().c_str() );
    return true;
}

String
Config::toString() {
    INSTRUMENTATION
    StaticJsonDocument<200> conf;

    conf[ F("name")     ] = name;
    conf[ F("freq_rt")  ] = freq_rt;
    conf[ F("freq_net") ] = freq_net;
    conf[ F("freq_sd")  ] = freq_sd;

    // Network Settings
    conf[ F("mac")      ] = mac2string( mac );
    conf[ F("ip")       ] = String( ip );
    conf[ F("dns")      ] = String( dns );
    conf[ F("gateway")  ] = String( gateway );
    conf[ F("subnet")   ] = String( subnet );

    String output;
    serializeJsonPretty( conf, output );
    return output;
}
