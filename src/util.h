#ifndef UTIL_H
#define UTIL_H

#include <RTClib.h>
#include <ArduinoJson.h>
#include <Ethernet.h>

String timeString( const DateTime &dateTime );
String dateString( const DateTime &dateTime );
String spanString( const TimeSpan &span );
String uint8bits( const uint8_t &mask );
String mac2str( uint8_t *mac );

String bin2str( const uint8_t &bits );

// converts a mask to an integer or -1 for failure.
int bitPos( const uint8_t &mask );

// finds the location of the first 0 bit in a mask
int firstEmptyBitNum( const uint8_t &mask );

// gives the bitmask of the the first available bit.
uint8_t firstEmptyBit( const uint8_t &mask );

#endif //UTIL_H
