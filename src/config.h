#ifndef CONFIG_H
#define CONFIG_H

#include <Ethernet.h>
#include <lumberjack.h>
namespace LJ = LumberJack;

/*
 * Configuration Library Notes
 *
 * TODO this may no longer be appropriate to have it as a singleton, as each
 * periphery will require its own configuration, that means that there will be
 * multiple instances of a configuration object. something to think about
 * moving into the future.
 *
 * TODO I've had thoughts that make me think this might be good to just keep as
 * a json object instead of turning it into one for saving and loading.
 */
class Config {
/* Singleton boilerplate */
public:
    static Config& get(){
        static Config instance;
        return instance;
    }
private:
    Config() {}
    ~Config() {}

public:
    Config( Config const& )     = delete;
    void operator=( Config const& ) = delete;

/* Configuration implementation */

    bool load();
    bool save();
    String toString();

/* variables */
//System
    String name = "Hyperion";
    String conf_name = "conf.json";
    LJ::LogLevel log_level = LJ::LogLevel::DEBUG;
    unsigned int freq_rt = 15;
    unsigned int freq_net = 250;
    unsigned int freq_sd = 6000;
//Output
//FIXME replace with an enum
    bool log_sp = true;
    bool log_ws = true;
    bool log_sd = true;
    String log_name = "hyperion.log";
//Serial
    uint16_t sp_baud = 9600;
//Network
    uint8_t mac[6]   { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
    IPAddress ip     {  10,   0,   0, 177 };
    IPAddress dns    {  10,   0,   0,   1 };
    IPAddress gateway{  10,   0,   0,   1 };
    IPAddress subnet { 255, 255,   0,   0 };
};
#endif //CONFIG_H
