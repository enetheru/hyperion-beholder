#include <SdFat.h>
#include <lumberjack.h>
#include "instrumentation.h"
#include "sdcontrol.h"

using namespace LumberJack::LOGGERS;

#define SPI_SPEED SD_SCK_MHZ(50)
extern SdFat SD;

bool
SDEnable( const int chip_select = 4){
    INSTRUMENTATION

    LOGI( F("Setting Up SD Card") );
    if( !SD.begin( 4, SPI_SPEED ) ){
        SD.initErrorPrint();
        LOGE( F("initialization failed") );
        if( SD.cardErrorCode() ){
            LOGI( F("errorCode: "), String( SD.cardErrorCode(), HEX) );
            LOGI( F("errorData: "), String( SD.cardErrorData(), HEX) );
            return false;
        }
        if( SD.fatType() == 0 ){
            LOGE( F("Can't find a valid FAT16/FAT32 partition.\n") );
            return false;
        }
        if( !SD.vwd()->isOpen() ){
          LOGE( F("Can't open root directory.\n") );
          return false;
        }
        LOGE( F("Can't determine error type\n") );
        return false;
    }
    LOGI( F("SDCARD OK") );

    return true;
}

bool
SDIsEnabled() {
    return !SD.cardErrorCode() && SD.vwd()->isOpen();
}

bool
SDDisable(){
    LOGI( F("Disabling SD Card") );
    SD.vwd()->close();
    return true;
}
