#ifndef PERIPHERAL_H
#define PERIPHERAL_H
/*
 * This is the base class for all peripherals
 * it provides the template for what is required per periphery
 *
 */
#include <Arduino.h>
#include <ArduinoJson.h>

class Peripheral {
public:
    Peripheral( const String &name ) : name( name ) {}

    // any setup code goes here
    virtual void setup() = 0;

    // perform any operations to get the data required.
    virtual void eval() {
        unsigned long this_ms = millis();

        if( this_ms - rt_ms > rt_freq ){
            rt = true;
            rt_ms = this_ms;
        } else rt = false;

        if( this_ms - sd_ms > sd_freq ){
            sd = true;
            sd_ms = this_ms;
        } else sd = false;
    }

    // generate reports
    virtual void report_rt( JsonDocument &doc ) = 0;
    virtual void report_sd( JsonDocument &doc ) = 0;

    // a way to react to input
    virtual void request( JsonDocument &data )  = 0;

    String name;

    bool rt = false;
    unsigned long rt_ms = 0;
    unsigned long rt_freq = 1000/25;       // 25fps.

    bool sd = false;
    unsigned long sd_ms = 0;
    unsigned long sd_freq = 60000;    // 1 Minute;

};

#endif// PERIPHERAL_H
