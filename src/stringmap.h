#pragma once

template<typename T>
struct Pair {
    String first;
    T second;
};

template<class T,int N>
struct StringMap {
    Pair<T> list[N];

    //Rudimentary linear search
    bool find(String name, T& ret ){
        for( auto i : list){
            if( i.first.equals(name) ){
                ret = i.second;
                return true;
            }
        }
        return false;
    }
};

/* Example of use
struct Parent{
    using FuncPair = Pair<(Parent::*)(int)>;
    void func1( int num ){}
    void func2( int num ){}
    void func3( int num ){}

    Map<void (Parent::*)(int),3> map{{
        FuncPair{"func1",&Parent::func1},
        FuncPair{"func2",&Parent::func2},
        FuncPair{"func3",&Parent::func3}
    }};

    void call(String name){
        auto func = map.find(name);
        (this->*func)( 1 );
    }
};
*/
