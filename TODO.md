TODO
====
- [ ] Create a page to monitor just the system output
  peripherals.
- [ ] make the websocket output selective with subscriptions
- [ ] flesh out help documentation
    - [ ] generate help page dynamically by querying the beholder and related
    - recursively delete all files
    - delete empty directories
    - update all html files from source tree
    - send http commands to peripherals
- [ ] extend configuration code to individually manage peripherals
- [DONE] Create a set of javascript functions that load resources on demand,
      without re-loading unnecessarily.
    SOLUTION: was to use es6 modules, i did initially create a fetch based
        script loader, but during that process learned about modules and
        changed over my workflow to use them instead
- [ ] create widgets for variable display and editing
- [ ] Fix logging
    - syslog size is not managed
    - sensorlog naming is not robust
    - sensorlog files clobber too quickly
- [ ] Replace lumberjack or make constexpr or something i think it bloats the
      binary, template instantiation can grow out of control
- [ ] modularise logging
- [ ] modularise sd card
    - implement sd card websocket streams
- [ ] mdularise network
    - implement network monitoring websockeet streams and reporting
- [ ] fix log aggregation on the aggregate server
- [ ] implement aggregate interface building
- [ ] implement aggregate websocket proxy
- [ ] clean up graph and extend
    - multiple plotted variables
    - annotations
    - pause transitions whilst not being viewed or disabled
    - make pretty
    - legends
    - it has its own TODO list so check that out and consolidate.
- [ ] implement more sensors
    - specifically a humidity and temperature controlled exhaust fan
- [ ] implement aggregate realtime data analysis and action based framework
- [ ] evaluate mqtt and see how it might fit into my work
- [ ] re-do interface so it works within different viewing contexts
- [ ] make user interface pretty like blender
- [ ] remove http section as it is unlikely to be useful.
