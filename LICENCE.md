No licence, which equates to all rights reserved, but I use various other
projects in the formation of this software, so whatever meets the requirements
of those projects.

I'm happy to re-evaluate of anyone wants me to. but it will require a review of
all the components to make sure that the resultant licence conforms to their
requirements.
